# theatreteacher.org

This webapp is intended as a resource for theatre teachers everywhere.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.  You will also need to start the API server (see below) for the application to work.

## API server

Run `npm server` to start the API server.  You wil need a `.env` file in the root project directory before starting the API server.  Ask a team member for this file.  Note that you need to kill and restart the server after making changes to the API server, there is no live-reloading for server files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running production mirror

Run `npm run build`, then navigate to the dist folder and run `npm run start:prod` to run the application.  In production mode, the server will server the front-end application via SSR.

## Running front-end unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Running server tests

Coming soon.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
