import { Injectable } from '@angular/core';
import { EnvironmentService } from './environment.service';

@Injectable({
  providedIn: 'root'
})
export class log {

  constructor() { }

  public static debug(...messages) {
    if (EnvironmentService.isLocal()) {
      console.log(...messages);
    }
  }

  public static info(...messages) {
    console.log(...messages);
  }

  public static error(message, error?) {
    if (!error) {
      if (EnvironmentService.isLocal()) {
        console.error(message);
      }
    } else {
      console.error(message);
      if (EnvironmentService.isLocal()) {
        console.error(error);
      }
    }
  }
}
