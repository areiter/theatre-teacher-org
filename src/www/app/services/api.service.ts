import { Injectable } from '@angular/core';
import { Character, Lesson, NewUser, Play, Problem, Scene, ServerPlay, User } from '../common/models';
import { Observable } from 'rxjs';
import { RefreshHttpClient } from './refresh-http-client.service';
import { map } from 'rxjs/operators';
import { CacheService } from './cache.service';
import { HttpClient } from '@angular/common/http';
import { ResetModel } from '../views/account/reset/reset.component';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
    private rfhttp: RefreshHttpClient,
    private cacheService: CacheService,
  ) { }

  // Auth
  login(credentials) {
    return this.http.post('/api/authentication/login', credentials, {
      observe: 'response',
      headers: this.rfhttp.createApiKeyHeaders()
    }).pipe(map((response) => {
      let body: any = response.body;
      if (body['id'] && response.headers.get('accesstoken')) {
        this.cacheService.setAccessToken(response.headers.get('accesstoken'));
        this.cacheService.setRefreshToken(response.headers.get('refreshtoken'));
        this.cacheService.setUser(body);
      }

      return response.body;
    }));
  }

  logout() {
    return this.rfhttp.post('/api/authentication/logout', {
      refreshToken: this.cacheService.getRefreshToken()
    }).pipe(map((response) => {
      this.cacheService.clearAccessToken();
      this.cacheService.clearRefreshToken();
      this.cacheService.clearUser();

      return response;
    }));
  }

  resetPassword(username):Observable<any> {
    return this.rfhttp.post(`/api/authentication/reset-password`, {username});
  }

  validateResetToken(token:string):Observable<any> {
    return this.rfhttp.post(`/api/authentication/validate-token`, {token});
  }

  changePassword(model:ResetModel):Observable<any> {
    return this.rfhttp.post(`/api/authentication/change-password`, model);
  }

  updatePassword(password:string):Observable<any> {
    return this.rfhttp.post(`/api/authentication/update-password`, {password});
  }

  // User
  createUser(newUser: NewUser): Observable<User> {
    return this.rfhttp.put(`/api/user`, newUser);
  }

  deleteAccount(): Observable<any> {
    return this.rfhttp.post(`/api/user/delete`, {});
  }

  // Play
  readUserPlays(): Observable<any[]> {
    return this.rfhttp.get(`/api/play/my-list`);
  }

  readLessons(): Observable<Lesson[]> {
    return this.rfhttp.get(`/api/play/lessons`);
  }

  readScenes(lessonId?:string): Observable<Scene[]> {
    const lid = lessonId? `?lesson=${lessonId}` : '';
    return this.rfhttp.get(`/api/play/scenes${lid}`);
  }

  readCharacters(lessonId?:string): Observable<Character[]> {
    const lid = lessonId? `?lesson=${lessonId}` : '';
    return this.rfhttp.get(`/api/play/characters${lid}`);
  }

  readProblems(lessonId?:string): Observable<Problem[]> {
    const lid = lessonId? `?lesson=${lessonId}` : '';
    return this.rfhttp.get(`/api/play/problems${lid}`);
  }

  printPlay(play:Play): Observable<{html: string}> {
    return this.rfhttp.post(`/api/play/print`, {play});
  }

  savePlay(play:ServerPlay): Observable<ServerPlay> {
    return this.rfhttp.post(`/api/play/save`, {play});
  }

  deletePlay(playId:string): Observable<any> {
    return this.rfhttp.post(`/api/play/delete`, {playId});
  }
}
