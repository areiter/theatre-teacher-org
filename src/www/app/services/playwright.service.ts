import { Injectable } from '@angular/core';
import { BehaviorSubject, of, ReplaySubject } from 'rxjs';
import { Character, Lesson, Play, PlayLine, PlayState, Problem, Scene, ServerPlay } from '../common/models';
import { pairwise, shareReplay, startWith, switchMap, take, tap } from 'rxjs/operators';
import { log } from './log.service';
import { ApiService } from './api.service';
import { CacheService } from './cache.service';
import { nanoid } from 'nanoid';
import { ErrorService } from './error.service';
import { ConfirmService } from './confirm.service';
import { Router } from '@angular/router';
import { distinctUntilChanged, filter, map } from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class PlaywrightService {

  get defaultPlay():Play {
    return {
      id: nanoid(8),
      derivedId: null,
      user: null,
      sharedWith: [],
      name: 'Untitled Play',
      scene: null,
      characters: [],
      problem: null,
      lines: [],
      lesson: undefined,
      state: PlayState.IN_PROGRESS
    };
  }

  readonly activePlay$:BehaviorSubject<Play>;
  readonly lessonChange$:ReplaySubject<any>;

  readonly lessons$: ReplaySubject<Lesson[]>;
  readonly scenes$: BehaviorSubject<Scene[]>;
  readonly characters$: BehaviorSubject<Character[]>;
  readonly problems$: ReplaySubject<Problem[]>;
  readonly selectedCharacter$: BehaviorSubject<Character>;
  readonly saving$: BehaviorSubject<boolean>;
  readonly unsaved$: BehaviorSubject<boolean>;

  constructor(
    private readonly apiService: ApiService,
    private readonly cacheService: CacheService,
    private readonly errorService: ErrorService,
    private readonly confirmService: ConfirmService,
    private readonly router: Router
  ) {
    this.activePlay$ = new BehaviorSubject<Play>(this.cacheService.getCachedPlay() || null);
    this.lessonChange$ = new ReplaySubject<any>();

    this.activePlay$.pipe(
      filter(Boolean),
      map(p => (p as Play).lesson),
      distinctUntilChanged(),
      tap(v => log.debug('lesson change', v)),
    ).subscribe(this.lessonChange$);

    this.lessons$ = new ReplaySubject<Lesson[]>();
    this.scenes$ = new BehaviorSubject<Scene[]>(null);
    this.characters$ = new BehaviorSubject<Character[]>(null);
    this.problems$ = new ReplaySubject<Problem[]>();
    this.selectedCharacter$ = new BehaviorSubject<Character>(null);
    this.saving$ = new BehaviorSubject<boolean>(false);
    this.unsaved$ = new BehaviorSubject<boolean>(false);

    this.apiService.readLessons().pipe(
      take(1),
      tap(lessons => log.debug('available lessons', lessons)),
      shareReplay(1)
    ).subscribe(this.lessons$);

    this.lessonChange$.pipe(
      filter(v => v !== undefined),
      switchMap(v => this.apiService.readScenes(v).pipe(take(1))),
      tap(scenes => log.debug('available scenes', scenes)),
      shareReplay(1)
    ).subscribe(this.scenes$);

    this.lessonChange$.pipe(
      switchMap(v => {
        if (v) {
          return this.apiService.readCharacters(v).pipe(take(1));
        } else {
          return of([]);
        }
      }),
      tap(characters => log.debug('available characters', characters)),
      shareReplay(1)
    ).subscribe(this.characters$);

    // this.apiService.readProblems().pipe(
    //   take(1),
    //   tap(problems => log.debug('available problems', problems)),
    //   shareReplay(1)
    // ).subscribe(this.problems$);

    this.lessonChange$.pipe(
      switchMap(v => this.apiService.readProblems(v).pipe(take(1))),
      tap(problems => log.debug('available problems', problems)),
      shareReplay(1)
    ).subscribe(this.problems$);

    this.activePlay$.pipe(
      startWith(null),
      pairwise()
    ).subscribe(([prev, play]) => {
      if (play) {
        if (prev) {
          this.unsaved$.next(true);
        }
        this.cacheService.setCachedPlay(play);
      } else {
        this.unsaved$.next(false);
        this.cacheService.clearCachedPlay();
      }
    });
  }

  startNew() {
    const play = {...this.defaultPlay};
    this.openPlay(play);
  }

  startNewCache() {
    this.activePlay$.next({...this.defaultPlay});
  }

  openPlay(play:Play) {
    const activePlay = this.activePlay$.getValue();
    if (activePlay && activePlay.id !== play.id && this.unsaved$.getValue()) {
      this.confirmService.show('You have a play in progress with unsaved changes.  If you open another play, any changes you have in progress will be lost.  Are you sure you want to continue?', () => {
        this.editPlay(play);
      });
    } else {
      this.editPlay(play);
    }
  }

  private editPlay(play:Play) {
    this.activePlay$.next(play);
    switch (true) {
      case (!play.lesson && play.lesson !== null): return this.router.navigate(['/playwrights-pal/lesson']);
      case (!play.scene): return this.router.navigate(['/playwrights-pal/new-play/setting']);
      case (!play.characters || play.characters.length === 0): return this.router.navigate(['/playwrights-pal/new-play/characters']);
      case (!play.problem): return this.router.navigate(['/playwrights-pal/new-play/problem']);
      default: this.router.navigate(['/playwrights-pal/script-writer/title']);
    }
  }

  setLesson(lesson:Lesson) {
    const play = this.activePlay$.getValue();
    if (play.lesson === undefined ||
      (play.lesson === null && !lesson) ||
      (play.lesson !== null && lesson !== null && play.lesson === lesson.id)
    ) {
      play.lesson = lesson ? lesson.id : null;
      this.activePlay$.next(play);
      this.editPlay(play);
    } else {
      this.confirmService.show('You have a play in progress from a different lesson.  If you start a new play, your old play will be lost.  Are you sure?', () => {
        const play = {...this.defaultPlay};
        play.lesson = lesson ? lesson.id : null;
        this.activePlay$.next(play);
        this.editPlay(play);
      });
    }
  }

  setScene(scene:Scene) {
    const play = this.activePlay$.getValue();
    play.scene = {...scene};
    this.activePlay$.next(play);
  }

  toggleCharacter(character:Character) {
    const play = this.activePlay$.getValue();
    const index = play.characters.findIndex(c => c.id === character.id);
    if (index >= 0) {
      play.characters.splice(index, 1);
    } else {
      play.characters.push({...character});
    }
    this.activePlay$.next(play);
  }

  removeCharacter(index:number) {
    const play = this.activePlay$.getValue();
    if (index >= 0 && play.characters.length > index) {
      const characterId = play.characters[index].id;
      play.characters.splice(index, 1);
      play.lines = play.lines.filter(line => !line.character || line.character.id !== characterId);
      this.activePlay$.next(play);
    }
  }

  addCharacter(character:Character) {
    const play = this.activePlay$.getValue();
    play.characters.push({...character, id: nanoid(), templateId: character.id});
    this.activePlay$.next(play);
  }

  setProblem(problem:Problem) {
    const play = this.activePlay$.getValue();
    play.problem = {...problem};
    this.activePlay$.next(play);
  }

  setTitle(title:string) {
    const play = this.activePlay$.getValue();
    play.name = title;
    this.activePlay$.next(play);
  }

  updateCharacter(character:Character) {
    const play = this.activePlay$.getValue();
    const index = play.characters.findIndex(c => c.id === character.id);
    if (index >= 0) {
      play.characters[index] = {...character};
    } else {
      play.characters.push({...character});
    }
    play.lines.forEach(line => {
      if (line.character && line.character.id === character.id) {
        line.character = character;
      }
    });
    this.activePlay$.next(play);
  }

  setLines(lines:PlayLine[]) {
    const play = this.activePlay$.getValue();
    play.lines = lines;
    this.activePlay$.next(play);
  }

  addLine(line:PlayLine) {
    const play = this.activePlay$.getValue();
    if (!line.id) {
      line.id = nanoid(8);
    }
    play.lines.push(line);
    this.activePlay$.next(play);
  }

  removeLine(line:PlayLine) {
    const play = this.activePlay$.getValue();
    const index = play.lines.findIndex(l => l.id === line.id);
    if (index >= 0) {
      play.lines.splice(index, 1);
      this.activePlay$.next(play);
    }
  }

  editLine(line:PlayLine) {
    const play = this.activePlay$.getValue();
    const index = play.lines.findIndex(l => l.id === line.id);
    if (index >= 0) {
      play.lines[index].text = line.text;
      play.lines[index].character = line.character;
      play.lines[index].type = line.type;
      this.activePlay$.next(play);
    }
  }

  moveLine(line:PlayLine, newIndex:number) {

  }

  clearActivePlay() {
    this.activePlay$.next(null);
  }

  save() {
    const saving = this.saving$.getValue();
    if (saving) {
      return false;
    }

    const play = this.activePlay$.getValue();
    const serverPlay = new ServerPlay(play, this.cacheService.getUser());
    this.saving$.next(true);
    let sub = this.apiService.savePlay(serverPlay).subscribe(
      result => {
        sub.unsubscribe();
        play.state = result.state as PlayState;
        play.id = result.id as string;
        this.activePlay$.next(play);
        this.saving$.next(false);
        this.unsaved$.next(false);
        log.debug('play saved');
      },
      error => {
        this.errorService.handle(error);
        sub.unsubscribe();
        this.saving$.next(false);
      }
    )
  }

  parseReadServerPlay(p:any):Play {
    const play = {
      id: null,
      derivedId: null,
      user: null,
      sharedWith: null,
      name: null,
      scene: null,
      characters: null,
      problem: null,
      lines: null,
      state: null
    };

    play.id = p.id;
    play.user = p.user;
    play.sharedWith = p.sharedWith || [];
    play.name = p.name;
    play.state = p.state;

    if (p.scene) {
      play.scene = {
        ...p.scene.template,
        name: p.scene.name,
        description: p.scene.description
      };
    }

    if (p.problem) {
      play.problem = {
        ...p.problem.template,
        name: p.problem.name,
        description: p.problem.description
      };
    }

    if (p.characters) {
      play.characters = p.characters.map(c => {
        return {
          ...c.template,
          id: c._id,
          templateId: c.template.id,
          name: c.name,
          description: c.description
        };
      });
    } else {
      play.characters = [];
    }

    if (p.lines) {
      play.lines = p.lines.map(l => {
        return {
          type: l.type,
          text: l.text,
          character: play.characters[l.characterIndex]
        }
      });
    } else {
      play.lines = [];
    }

    return play;
  }

  print(play?: Play) {
    if (!play) {
      play = this.activePlay$.getValue();
    }

    let sub = this.apiService.printPlay(play).subscribe(
      result => {
        sub.unsubscribe();
        const printWindow = window.open('about:blank', '', '_blank');
        printWindow.document.write(result.html);
      },
      error => {
        sub.unsubscribe();
        this.errorService.handle(error);
      }
    )
  }
}
