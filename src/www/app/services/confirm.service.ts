import { Injectable } from '@angular/core';
import {NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {ConfirmModalComponent} from "./confirm-modal.component";

function actionsIsSingleFunction(actions: unknown): actions is Function {
  return actions instanceof Function;
}

function actionsIsMultiFunction(actions: unknown): actions is {complete: Function, cancel: Function} {
  return actions['complete'] instanceof Function && actions['cancel'] instanceof Function;
}

@Injectable({
  providedIn: 'root'
})
export class ConfirmService {

  constructor(
    private modalService:NgbModal
  ) { }

  public show(text: string, actions?: unknown, modalOptions?: NgbModalOptions): void {
    let ref = this.modalService.open(ConfirmModalComponent, modalOptions);
    ref.componentInstance.text = text;

    ref.result.then(() => {
      if (actionsIsSingleFunction(actions)) {
        actions();
      } else if (actionsIsMultiFunction(actions)) {
        actions.complete();
      } else {
        throw new Error(`Could not resolve action from actions: ${actions}!`);
      }
    }, (err) => {
      if (actionsIsMultiFunction(actions)) {
        actions.cancel();
      }
    });
  }
}
