import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'iok-confirm-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-basic-title">
        <fa-icon [icon]="faQuestionCircle" class="mr-2"></fa-icon>{{title}}
      </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>{{text}}</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" (click)="activeModal.close('Done')">Confirm</button>
      <button type="button" class="btn btn-secondary" (click)="activeModal.dismiss('Cancel')">Cancel</button>
    </div>`
})
export class ConfirmModalComponent implements OnInit {

  @Input() title:string = 'Confirm';
  @Input() text:string = 'Are you sure?';

  faQuestionCircle = faQuestionCircle;

  constructor(
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

}
