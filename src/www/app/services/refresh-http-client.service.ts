import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { CacheService } from './cache.service';
import { log } from './log.service';

@Injectable()
export class RefreshHttpClient {

  queue: any[] = [];
  refreshing:boolean = false;

  constructor(
    private httpClient: HttpClient,
    private cacheService: CacheService
  ) {}

  createAuthorizationHttpHeaders(): HttpHeaders {
    const accessToken = this.cacheService.getAccessToken();
    return new HttpHeaders().set('Authorization', 'Bearer ' + accessToken);
  }

  createApiKeyHeaders() {
    return new HttpHeaders().set('apikey', 'abc123');
  }

  getSimple(url) {
    return this.httpClient.get(url, {
      headers: this.createAuthorizationHttpHeaders()
    });
  }

  client() {
    return this.httpClient;
  }

  private runRequest(req, done) {
    let httpRequest;
    if (req.method === 'get' || req.method === 'delete') {
      httpRequest = this.httpClient[req.method](req.url, {
        headers: this.createAuthorizationHttpHeaders()
      });
    } else {
      httpRequest = this.httpClient[req.method](req.url, req.body, {
        headers: this.createAuthorizationHttpHeaders()
      });
    }

    const obs = httpRequest
      .subscribe(
        data => {
          obs.unsubscribe();
          req.sub.next(data);
          return done();
        },
        error => {
          if (error && error.error && error.error.error_code === 4010) {
            if (this.refreshing) {
              log.debug('already refreshing, adding ' + req.url + ' to queue');
              this.queue.push(req);
            } else {
              log.debug('starting refresh and adding ' + req.url + ' to queue');
              this.refreshing = true;
              this.queue.push(req);
              this.refresh(() => {
                this.refreshing = false;
              });
            }
          } else {
            obs.unsubscribe();
            req.sub.error(error);
            return done();
          }
        }
      );
  }

  private runUploadRequest(req, done) {
    let httpRequest;
    const options: any = {
      headers: this.createAuthorizationHttpHeaders(),
      reportProgress: true
    };

    httpRequest = new HttpRequest(req.method, req.url, req.body, options);

    this.refresh(() => {
      const obs = this.httpClient.request(httpRequest)
        .subscribe(
          event => {
            if (event instanceof HttpResponse) {
              obs.unsubscribe();
            }
            req.sub.next(event);
            return done();
          },
          error => {
            obs.unsubscribe();
            req.sub.error(error);
            return done();
          });
    });
  }

  private runImageRequest(req, done) {
    const obs = this.httpClient.get(req.url, {
      headers: this.createAuthorizationHttpHeaders(),
      responseType: 'blob'
    })
      .subscribe(
        data => {
          obs.unsubscribe();
          req.sub.next(data);
          return done();
        },
        error => {
          if (error.status === 403) {
            if (this.refreshing) {
              log.debug('already refreshing, adding ' + req.url + ' to queue');
              this.queue.push(req);
            } else {
              log.debug('starting refresh and adding ' + req.url + ' to queue');
              this.refreshing = true;
              this.queue.push(req);
              this.refresh(() => {
                this.refreshing = false;
              });
            }
          } else {
            obs.unsubscribe();
            req.sub.error(error);
            return done();
          }
        }
      );
  }

  refresh(callback) {
    this.refreshing = true;
    const refSub = this.httpClient
      .post('/api/authentication/refresh', { refreshToken: this.cacheService.getRefreshToken() }, {
        observe: 'response',
        headers: this.createApiKeyHeaders()
      })
      .subscribe(
        response => {
          this.cacheService.setAccessToken(response.headers.get('accesstoken'));

          log.debug('refreshed session');
          this.runQueue();
          refSub.unsubscribe();
          //this.refreshing = false;
          callback();
        },
        error => {
          log.error('refresh failed', error);
          refSub.unsubscribe();
          this.queue = [];

          //this.eventService.broadcast('fire-exit');
        }
      );
  }

  private runQueue() {
    if (this.queue.length === 0) {
      log.debug('queue empty, exiting');
      return;
    } else {
      log.debug('queue length is ' + this.queue.length);
      let run;
      let done;
      const req = this.queue[0];
      done = () => {
        log.debug(req.url + ' done');
        this.queue.shift();
        if (this.queue.length > 0) {
          run(this.queue[0]);
        }
      };
      run = (r) => {
        log.debug('running ' + req.url + ' from queue');
        if (r.image) {
          this.runImageRequest(r, done);
        } else if (r.upload) {
          this.runUploadRequest(r, done);
        } else {
          this.runRequest(r, done);
        }
      };
      run(req);
    }
  }

  get(url): Observable<any> {
    const req = {
      url,
      method: 'get',
      image: false,
      body: null,
      sub: new Subject<any>()
    };
    if (this.refreshing) {
      this.queue.push(req);
      this.runQueue();
    } else {
      this.runRequest(req, () => {});
    }

    return req.sub.asObservable();
  }

  getImage(url): Observable<any> {
    if (this.queue.length > 0) {
      // If request is already running, return that one
      const existingReq = this.queue.find((r) => {
        return r.image === true && r.url === url;
      });
      if (existingReq) {
        return existingReq.sub.asObservable();
      }
    }
    const req = {
      url,
      method: 'get',
      image: true,
      body: null,
      sub: new Subject<any>()
    };
    if (this.refreshing) {
      this.queue.push(req);
      this.runQueue();
    } else {
      this.runImageRequest(req, () => {});
    }

    return req.sub.asObservable();
  }

  upload(url, data) {
    const req = {
      url,
      method: 'post',
      image: false,
      upload: true,
      body: data,
      sub: new Subject<any>()
    };
    if (this.refreshing) {
      this.queue.push(req);
      this.runQueue();
    } else {
      this.runUploadRequest(req, () => {});
    }

    return req.sub.asObservable();
  }

  post(url, data) {
    const req = {
      url,
      method: 'post',
      image: false,
      body: data,
      sub: new Subject<any>()
    };
    if (this.refreshing) {
      this.queue.push(req);
      this.runQueue();
    } else {
      this.runRequest(req, () => {});
    }

    return req.sub.asObservable();
  }

  put(url, data) {
    const req = {
      url,
      method: 'put',
      image: false,
      body: data,
      sub: new Subject<any>()
    };
    if (this.refreshing) {
      this.queue.push(req);
      this.runQueue();
    } else {
      this.runRequest(req, () => {});
    }

    return req.sub.asObservable();
  }

  delete(url) {
    const req = {
      url,
      method: 'delete',
      image: false,
      body: null,
      sub: new Subject<any>()
    };
    if (this.refreshing) {
      this.queue.push(req);
      this.runQueue();
    } else {
      this.runRequest(req, () => {});
    }

    return req.sub.asObservable();
  }
}
