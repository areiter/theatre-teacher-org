import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'iok-error-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title red" id="modal-basic-title">
        <fa-icon [icon]="faExclamationCircle" class="mr-2"></fa-icon>Error
      </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>{{text}}</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" (click)="activeModal.close('Done')">Ok</button>
    </div>`
})
export class ErrorModalComponent implements OnInit {

  @Input() text:string = "A server error occurred.  That's probably out fault.  Sorry about that!  Let us know what happened and we'll fix it right away.";

  faExclamationCircle = faExclamationCircle;

  constructor(
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

}
