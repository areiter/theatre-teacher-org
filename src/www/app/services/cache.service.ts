import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { log } from './log.service';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { Play, User } from '../common/models';

@Injectable()
export class CacheService {

  private ephemeral:any = {};
  readonly user$:BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor(
    @Inject(PLATFORM_ID) private platformId
  ) {
    const user = this.getUser();
    if (user) {
      this.user$.next(user);
    } else {
      this.user$.next(null);
    }
  }

  set(key: string, value: any) {
    localStorage.setItem(key, value);
  }

  get(key: string): any {
    return localStorage.getItem(key);
  }

  clear(key: string) {
    localStorage.removeItem(key);
  }

  setEphemeral(key: string, value: any) {
    this.ephemeral[key] = value;
  }

  getEphemeral(key: string): any {
    return this.ephemeral[key];
  }

  getCachedPlay():Play {
    return JSON.parse(localStorage.getItem('tto-play'));
  }

  setCachedPlay(play:Play) {
    localStorage.setItem('tto-play', JSON.stringify(play));
  }

  clearCachedPlay() {
    localStorage.removeItem('tto-play');
  }

  getUser(): any {
    if (isPlatformBrowser(this.platformId)) {
      return JSON.parse(localStorage.getItem('tto-user'));
    } else {
      return null;
    }
  }

  setUser(user) {
    if (isPlatformBrowser(this.platformId)) {
      log.debug('User ' + user.id + ' stored in cache.');
      localStorage.setItem('tto-user', JSON.stringify(user));
      this.user$.next(user);
    } else {
      log.debug('Running on server, no local storage available');
    }
  }

  clearUser() {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.removeItem('tto-user');
      this.user$.next(null);
    } else {
      log.debug('Running on server, no local storage available');
    }
  }

  getAccessToken(): string {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem('tto-accessToken');
    } else {
      return null;
    }
  }

  setAccessToken(token) {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('tto-accessToken', token);
    } else {
      log.debug('Running on server, no local storage available');
    }
  }

  clearAccessToken() {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.removeItem('tto-accessToken');
    } else {
      log.debug('Running on server, no local storage available');
    }
  }

  getRefreshToken(): string {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem('tto-refreshToken');
    } else {
      return null;
    }
  }

  setRefreshToken(token) {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem('tto-refreshToken', token);
    } else {
      return null;
    }
  }

  clearRefreshToken() {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.removeItem('tto-refreshToken');
    } else {
      return null;
    }
  }

}
