import { Injectable } from '@angular/core';

@Injectable()
export class EnvironmentService {

  constructor() { }

  public static isLocal(): boolean {
    return window.location.hostname === 'localhost'
      || window.location.hostname.indexOf('test') !== -1;
  }

}
