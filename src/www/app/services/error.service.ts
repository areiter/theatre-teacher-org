import { Injectable } from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ErrorModalComponent} from "./error-modal.component";
import { log } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private modalService:NgbModal
  ) { }

  public handle(error, text?, completeAction?, modalOptions?) {
    log.error(error);

    let ref = this.modalService.open(ErrorModalComponent, modalOptions);
    if (text) {
      ref.componentInstance.text = text;
    } else if (error.error && error.error.error_description) {
      ref.componentInstance.text = error.error.error_description;
    }

    ref.result.then((result) => {
      if (completeAction && completeAction instanceof Function) {
        completeAction();
      }
    }, (err) => {
      if (completeAction && completeAction instanceof Function) {
        completeAction();
      }
    });
  }

  public show(text, completeAction?, modalOptions?) {
    let ref = this.modalService.open(ErrorModalComponent, modalOptions);
    ref.componentInstance.text = text;

    ref.result.then((result) => {
      if (completeAction && completeAction instanceof Function) {
        completeAction();
      }
    }, (err) => {
      if (completeAction && completeAction instanceof Function) {
        completeAction();
      }
    });
  }
}
