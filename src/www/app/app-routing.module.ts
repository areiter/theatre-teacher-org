import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './views/home/home.component';
import { ArtsIntegrationComponent } from './views/arts-integration/arts-integration.component';
import { AtHomeComponent } from './views/at-home/at-home.component';
import { AccountComponent } from './views/account/account.component';
import { NewAccountComponent } from './views/account/new-account/new-account.component';
import { LibraryComponent } from './views/account/library/library.component';
import { PlaywrightsPalComponent } from './views/playwrights-pal/playwrights-pal.component';
import { PlaySetupComponent } from './views/playwrights-pal/play-setup/play-setup.component';
import { ScriptWriterComponent } from './views/playwrights-pal/script-writer/script-writer.component';
import { SettingComponent } from './views/playwrights-pal/play-setup/setting/setting.component';
import { CharactersComponent } from './views/playwrights-pal/play-setup/characters/characters.component';
import { ProblemComponent } from './views/playwrights-pal/play-setup/problem/problem.component';
import { PlayTitleComponent } from './views/playwrights-pal/script-writer/play-title/play-title.component';
import { PlayCastComponent } from './views/playwrights-pal/script-writer/play-cast/play-cast.component';
import { PlayLinesComponent } from './views/playwrights-pal/script-writer/play-lines/play-lines.component';
import { ActivePlayResolve } from './resolve/active-play.resolve';
import { ActivePlayGuard } from './resolve/active-play.guard';
import { ResetComponent } from './views/account/reset/reset.component';
import { LessonComponent } from './views/playwrights-pal/lesson/lesson.component';
import { PerformancesComponent } from './views/performances/performances.component';
import { TeacherToolsComponent } from './views/teacher-tools/teacher-tools.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'arts-integration', component: ArtsIntegrationComponent},
  { path: 'artsintegration', redirectTo: 'arts-integration'},
  { path: 'in-the-classroom', redirectTo: 'arts-integration'},
  { path: 'at-home', component: AtHomeComponent},
  { path: 'athome', redirectTo: 'at-home'},
  { path: 'performances', component: PerformancesComponent},
  { path: 'teacher-tools', component: TeacherToolsComponent},
  { path: 'teachertools', redirectTo: 'teacher-tools'},
  { path: 'account', component: AccountComponent,
    children: [
      { path: 'new', component: NewAccountComponent },
      { path: 'library', component: LibraryComponent/*, canActivate: [UserAuthGuard]*/ },
      { path: 'reset', component: ResetComponent },
      { path: '**', redirectTo: 'library'}
    ]
  },
  { path: 'playwrights-pal', component: PlaywrightsPalComponent,
    children: [
      { path: 'lesson', component: LessonComponent, resolve: {activePlay: ActivePlayResolve }},
      { path: 'new-play', component: PlaySetupComponent, resolve: {activePlay: ActivePlayResolve},
        children: [
          { path: 'setting', component: SettingComponent },
          { path: 'characters', component: CharactersComponent },
          { path: 'problem', component: ProblemComponent },
          { path: '**', redirectTo: 'setting' },
        ]
      },
      { path: 'script-writer', component: ScriptWriterComponent, canActivate: [ActivePlayGuard],
        children: [
          { path: 'title', component: PlayTitleComponent },
          { path: 'cast', component: PlayCastComponent },
          { path: 'script', component: PlayLinesComponent },
          { path: '**', redirectTo: 'title' },
        ]
      },
      { path: '**', redirectTo: 'lesson'}
    ]
  },
  { path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
