import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeComponent } from './views/home/home.component';
import { ArtsIntegrationComponent } from './views/arts-integration/arts-integration.component';
import { AtHomeComponent } from './views/at-home/at-home.component';
import { AccountComponent } from './views/account/account.component';
import { LibraryComponent } from './views/account/library/library.component';
import { PerformancesComponent } from './views/performances/performances.component';
import { TeacherToolsComponent } from './views/teacher-tools/teacher-tools.component';
import { LineCreatorComponent } from './views/playwrights-pal/script-writer/play-lines/line-creator/line-creator.component';

import { PlaywrightsPalComponent } from './views/playwrights-pal/playwrights-pal.component';
import { PlaySetupComponent } from './views/playwrights-pal/play-setup/play-setup.component';
import { ScriptWriterComponent } from './views/playwrights-pal/script-writer/script-writer.component';
import { SettingComponent } from './views/playwrights-pal/play-setup/setting/setting.component';
import {
  CharactersComponent,
  SceneHasCharactersFilter
} from './views/playwrights-pal/play-setup/characters/characters.component';
import { ProblemComponent } from './views/playwrights-pal/play-setup/problem/problem.component';
import { PlayTitleComponent } from './views/playwrights-pal/script-writer/play-title/play-title.component';
import { PlayCastComponent } from './views/playwrights-pal/script-writer/play-cast/play-cast.component';
import { PlayLinesComponent } from './views/playwrights-pal/script-writer/play-lines/play-lines.component';
import { ResetComponent } from './views/account/reset/reset.component';
import { LessonComponent } from './views/playwrights-pal/lesson/lesson.component';
import { NewAccountComponent } from './views/account/new-account/new-account.component';

import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { MailingListLinkComponent } from './components/mailing-list-link/mailing-list-link.component';
import { QuoteComponent } from './components/quote/quote.component';
import { LoginComponent } from './components/login/login.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';

import { CacheService } from './services/cache.service';
import { EnvironmentService } from './services/environment.service';
import { RefreshHttpClient } from './services/refresh-http-client.service';
import { ErrorService } from './services/error.service';
import { ErrorModalComponent } from './services/error-modal.component';
import { ApiService } from './services/api.service';
import { ConfirmService } from './services/confirm.service';
import { ConfirmModalComponent } from './services/confirm-modal.component';

import { UserAuthGuard } from './resolve/guard.auth';
import { HumanizeDatePipe } from './pipes/humanize-date.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivePlayResolve } from './resolve/active-play.resolve';
import { ActivePlayGuard } from './resolve/active-play.guard';
import { PreloadImageDirective } from './directives/preload-image.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ArtsIntegrationComponent,
    FooterComponent,
    MailingListLinkComponent,
    AtHomeComponent,
    QuoteComponent,
    AccountComponent,
    NewAccountComponent,
    ErrorModalComponent,
    ConfirmModalComponent,
    LibraryComponent,
    LoginComponent,
    ResetPasswordComponent,
    ResetComponent,

    PlaywrightsPalComponent,
    LessonComponent,
    PlaySetupComponent,
    ScriptWriterComponent,
    SettingComponent,
    CharactersComponent,
    ProblemComponent,
    PlayTitleComponent,
    PlayCastComponent,
    PlayLinesComponent,
    LineCreatorComponent,

    HumanizeDatePipe,
    SceneHasCharactersFilter,
    PreloadImageDirective,
    PerformancesComponent,
    TeacherToolsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserModule.withServerTransition({ appId: 'theatre-teacher-org' }),
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    HttpClientModule,
    DragDropModule
  ],
  providers: [
    HttpClient,
    CacheService,
    EnvironmentService,
    RefreshHttpClient,
    ErrorService,
    ConfirmService,
    ApiService,
    UserAuthGuard,
    ActivePlayGuard,
    ActivePlayResolve
  ],
  entryComponents: [
    ErrorModalComponent,
    ConfirmModalComponent,
    LoginComponent,
    ResetPasswordComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private faLibrary:FaIconLibrary
  ) {
    faLibrary.addIconPacks(fas);
    faLibrary.addIconPacks(far);
    faLibrary.addIconPacks(fab);
  }
}
