import { Component } from '@angular/core';

@Component({
  selector: 'tto-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'theatre-teacher-org';
}
