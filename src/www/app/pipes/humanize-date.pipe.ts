import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'humanizeDate',
  pure: false
})
export class HumanizeDatePipe implements PipeTransform {
  transform(date: Parameters<typeof moment>[0]): string {

    const formats = {
      sameDay: '[Today at] h:mm A z',
      lastDay: '[Yesterday at] h:mm A z',
      lastWeek: '[Last] dddd [at] h:mm A z',
      sameElse: 'DD/MM/YYYY [at] h:mm A z'
    };
    return moment(date).calendar(null, formats);
  }
}
