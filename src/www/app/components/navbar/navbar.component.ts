import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { distinctUntilChanged, filter, map } from 'rxjs/internal/operators';

@Component({
  selector: 'tto-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit, AfterViewInit {

  page:String = null;
  showNav:boolean = false;

  constructor(
    private router:Router
  ) {
    this.router.events.pipe(
      filter(e => !!e['url']),
      map(e => e['url']),
      distinctUntilChanged()
    ).subscribe(url => {
      this.page = url;
    });
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    // const scroll$ = fromEvent(window, 'scroll').pipe(
    //   throttleTime(10),
    //   map(() => window.pageYOffset),
    //   pairwise(),
    //   map(([y1, y2]): Direction => (y2 < y1 ? Direction.Up : Direction.Down)),
    //   distinctUntilChanged(),
    //   share()
    // );
    //
    // const scrollUp$ = scroll$.pipe(
    //   filter(direction => direction === Direction.Up)
    // );
    //
    // const scrollDown$ = scroll$.pipe(
    //   filter(direction => direction === Direction.Down)
    // );
  }

  isPage(page) {
    return this.page && this.page.indexOf(page) === 0;
  }

}

enum Direction {
  Up = 'Up',
  Down = 'Down'
}
