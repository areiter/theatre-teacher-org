import {Component, ContentChild, Input, OnInit, ViewChild} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'tto-mailing-list-link',
  templateUrl: './mailing-list-link.component.html',
  styleUrls: ['./mailing-list-link.component.less']
})
export class MailingListLinkComponent implements OnInit {

  @Input() isButton:boolean = false;
  @Input() text:string;

  @ViewChild('mailingListModal', {static: false}) mailingListModal:NgbModal;

  constructor(
    private modalService:NgbModal
  ) { }

  ngOnInit() {
  }

  showMailingList() {
    this.modalService.open(this.mailingListModal).result.then(
      result => {},
      dismiss => {}
    )
  }

}
