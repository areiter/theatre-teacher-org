import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'tto-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.less']
})
export class QuoteComponent implements OnInit {

  @Input() text:string;
  @Input() direction:string = 'right';

  constructor() { }

  ngOnInit() {
  }

}
