import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'tto-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  @Input() modal: boolean = false;

  defaultModel = {
    username: null,
    password: null
  };
  model: LoginUser = {...this.defaultModel};

  submitting:boolean = false;
  error:string = null;

  constructor(
    public activeModal: NgbActiveModal,
    private apiService: ApiService
  ) { }

  ngOnInit() {
  }

  login() {
    this.submitting = true;
    this.error = null;
    let sub = this.apiService.login(this.model).subscribe(
      result => {
        sub.unsubscribe();
        this.activeModal.dismiss();
      },
      error => {
        this.submitting = false;
        sub.unsubscribe();
        this.model.password = null;
        this.error = error.error ? error.error.error_description : 'Login failed.';
      }
    )
  }

}

interface LoginUser {
  username: string,
  password: string
}
