import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'tto-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {

  @Input() white:boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
