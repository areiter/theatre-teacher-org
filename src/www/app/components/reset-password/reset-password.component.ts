import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../../services/api.service';
import { ErrorService } from '../../services/error.service';

@Component({
  selector: 'tto-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {

  @Input() modal: boolean = false;

  defaultModel = {
    username: null
  };
  model: ResetUser = {...this.defaultModel};

  submitting:boolean = false;
  error:string = null;
  success:string = null;

  constructor(
    public activeModal: NgbActiveModal,
    private apiService: ApiService,
    private errorService: ErrorService
  ) { }

  ngOnInit() {
  }

  reset() {
    this.submitting = true;
    let sub = this.apiService.resetPassword(this.model.username).subscribe(
      result => {
        sub.unsubscribe();
        //this.activeModal.dismiss();
        this.success = 'Success!  Please check your email for password reset instructions.';
      },
      error => {
        this.submitting = false;
        sub.unsubscribe();
        this.errorService.handle(error);
      }
    )
  }

}

interface ResetUser {
  username: string
}
