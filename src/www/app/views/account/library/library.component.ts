import { Component, OnInit, ViewChild } from '@angular/core';
import { CacheService } from '../../../services/cache.service';
import { Play, PlayState, User } from '../../../common/models';
import { Router } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { ErrorService } from '../../../services/error.service';
import { log } from '../../../services/log.service';
import { PlaywrightService } from '../../../services/playwright.service';
import { ConfirmService } from '../../../services/confirm.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'tto-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.less']
})
export class LibraryComponent implements OnInit {

  @ViewChild('newAccountModal', {static: false}) newAccountModal:NgbModal;

  wipPlay:Play;
  userPlays:Play[] = null;
  user:User;

  readonly wipUnsaved$ = this.playwrightService.unsaved$;

  constructor(
    private cacheService: CacheService,
    private router: Router,
    private apiService: ApiService,
    private errorService: ErrorService,
    private confirmService: ConfirmService,
    private playwrightService: PlaywrightService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    const cachedPlay = this.cacheService.getCachedPlay();
    if (cachedPlay) {
      this.wipPlay = cachedPlay;
    }

    this.cacheService.user$.subscribe(u => {
      this.user = u;
      if (this.user) {
        this.getUserPlays();
      } else {
        this.userPlays = [];
      }
    });

    this.playwrightService.activePlay$.subscribe(p => {
      this.wipPlay = p;
    });
  }

  startNewPlay() {
    this.playwrightService.startNew();
  }

  openPlay(play:Play) {
    this.playwrightService.openPlay(play);
  }

  printPlay(play:Play) {
    this.playwrightService.print(play);
  }

  clearCachedPlay() {

  }

  getUserPlays() {
    let sub = this.apiService.readUserPlays().subscribe(
      result => {
        sub.unsubscribe();
        this.userPlays = result
          .filter(p => !this.wipPlay || this.wipPlay && p.id !== this.wipPlay.id)
          .map(p => this.playwrightService.parseReadServerPlay(p));
        log.debug('user plays', this.userPlays);
      },
      error => {
        sub.unsubscribe();
        this.errorService.handle(error);
      }
    );
  }

  saveWip(play:Play) {
    if (!this.user) {
      this.modalService.open(this.newAccountModal).result.then(
        result => this.router.navigate(['/account/new']),
        reason => {}
      )
      return;
    }

    this.playwrightService.save();
    this.playwrightService.saving$.subscribe(
      saving => {
        if (!saving) {
          this.getUserPlays();
        }
      }
    );
  }

  removePlay(play:Play) {
    this.confirmService.show('Are you sure you want to delete this play?  You won\'t be able to recover it!', () => {
      play.deleting = true;
      if (this.wipPlay && play.id === this.wipPlay.id) {
        this.playwrightService.clearActivePlay();
        this.wipPlay = null;
      }
      if (play.state !== PlayState.IN_PROGRESS) {
        let sub = this.apiService.deletePlay(play.id).subscribe(
          result => {
            sub.unsubscribe();
            this.getUserPlays();
          },
          error => {
            play.deleting = false;
            sub.unsubscribe();
            this.errorService.handle(error);
          }
        )
      }
    });
  }

}
