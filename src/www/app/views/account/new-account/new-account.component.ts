import { Component, OnInit, ViewChild } from '@angular/core';
import { ErrorService } from '../../../services/error.service';
import { NewUser } from '../../../common/models';
import { ApiService } from '../../../services/api.service';
import { mergeMap, take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CacheService } from '../../../services/cache.service';
import { ConfirmService } from '../../../services/confirm.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'tto-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.less']
})
export class NewAccountComponent implements OnInit {

  @ViewChild('confirmEmailModal', {static: true}) confirmEmailModal:NgbModal;

  stage:number = 1;
  submitting:boolean = false;

  readonly emptyUser = {
    username: null,
    email: null,
    password: null,
    firstName: null,
    lastName: null,
    teacher: false
  };

  model: NewUser = {...this.emptyUser};
  submitted: false;

  constructor(
    private readonly errorService: ErrorService,
    private readonly apiService: ApiService,
    private readonly router: Router,
    private readonly cacheService: CacheService,
    private readonly modalService: NgbModal
  ) { }

  ngOnInit() {
    this.cacheService.user$.subscribe(
      user => {
        if (user) {
          this.router.navigate(['/account/library']);
        }
      }
    )
  }

  personalDone() {
    if (!this.model.email) {
      this.modalService.open(this.confirmEmailModal).result.then(
        result => {
          this.stage = 2;
        },
        reason => {}
      );
    } else {
      this.stage = 2;
    }
  }

  accountDone() {
    if (!this.model.firstName) {
      return this.errorService.show('Please enter your first name.');
    }

    if (!this.model.username) {
      return this.errorService.show('Please enter a username.');
    }

    if (!this.model.password) {
      return this.errorService.show('Please enter a password.');
    }

    if (this.model.password.length < 8) {
      return this.errorService.show('Your password must be 8 or more characters.');
    }

    this.submitting = true;
    let sub = this.apiService.createUser(this.model).pipe(
      take(1),
      mergeMap(user => this.apiService.login({username: this.model.username, password: this.model.password}).pipe(
        take(1)
      ))
    ).subscribe(
      result => {
        sub.unsubscribe();
        this.model = {...this.emptyUser};
        this.stage = 1;
        this.router.navigate(['/account/library']);
      },
      error => {
        sub.unsubscribe();
        this.submitting = false;
        this.errorService.handle(error);
      }
    )
  }

}
