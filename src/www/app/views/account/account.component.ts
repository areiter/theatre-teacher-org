import { Component, OnInit } from '@angular/core';
import { CacheService } from '../../services/cache.service';
import { User } from '../../common/models';
import { Router, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ApiService } from '../../services/api.service';
import { log } from '../../services/log.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../../components/login/login.component';
import { ResetPasswordComponent } from '../../components/reset-password/reset-password.component';
import { ConfirmService } from '../../services/confirm.service';
import { ErrorService } from '../../services/error.service';
import { PlaywrightService } from '../../services/playwright.service';

@Component({
  selector: 'tto-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.less']
})
export class AccountComponent implements OnInit {

  user: User;
  working: boolean;

  constructor(
    private cacheService: CacheService,
    private router: Router,
    private apiService: ApiService,
    private modalService: NgbModal,
    private confirmService: ConfirmService,
    private errorService: ErrorService,
    private playwrightService: PlaywrightService
  ) { }

  ngOnInit() {
    this.cacheService.user$.subscribe(user => this.user = user);
  }

  logout() {
    if (this.playwrightService.unsaved$.getValue()) {
      this.confirmService.show('Any unsaved work will be lost when you log out.  Are you sure you want to continue?', () => {
        this.confirmLogout();
      });
    } else {
      this.confirmLogout();
    }
  }

  confirmLogout() {
    this.working = true;
    let sub = this.apiService.logout().subscribe(
      result => {
        sub.unsubscribe();
        this.playwrightService.clearActivePlay();
        this.router.navigate(['/account/new']);
      },
      error => {
        log.error(error);
        sub.unsubscribe();

        this.cacheService.clearAccessToken();
        this.cacheService.clearRefreshToken();
        this.cacheService.clearUser();
        this.router.navigate(['/account/new']);
      }
    );
  }

  deleteAccount() {
    this.confirmService.show('Are you sure you want to permanently delete your account?  Your account and all your content will be deleted.  This CANNOT be undone!', () => {
      this.working = true;
      let sub = this.apiService.deleteAccount().subscribe(
        result => {
          sub.unsubscribe();
          log.debug('User deleted');
          this.cacheService.clearAccessToken();
          this.cacheService.clearRefreshToken();
          this.cacheService.clearUser();
          this.playwrightService.clearActivePlay();
          this.router.navigate(['/account']);
        },
        error => {
          sub.unsubscribe();
          this.errorService.handle(error);
        }
      )
    });
  }

  login() {
    const modalRef = this.modalService.open(LoginComponent);
    modalRef.componentInstance.modal = true;
    modalRef.result.then(
      result => {},
      reason => {}
    )
  }

  accountRecovery() {
    const modalRef = this.modalService.open(ResetPasswordComponent);
    modalRef.componentInstance.modal = true;
    modalRef.result.then(
      result => {},
      reason => {}
    )
  }

  changePassword() {

  }

}
