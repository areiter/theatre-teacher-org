import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { log } from '../../../services/log.service';
import { ErrorService } from '../../../services/error.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../../../components/login/login.component';
import { CacheService } from '../../../services/cache.service';
import { User } from '../../../common/models';

@Component({
  selector: 'tto-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.less']
})
export class ResetComponent implements OnInit {

  model:ResetModel = {
    password: null,
    confirmPassword: null,
    token: null
  }

  resetWorking: boolean = false;
  user:User = this.cacheService.getUser();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private errorService: ErrorService,
    private modalService: NgbModal,
    private cacheService: CacheService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (!this.user) {
        const token = params['t'];

        if (!token) {
          return this.router.navigate(['/account']);
        }

        let sub = this.apiService.validateResetToken(token).subscribe(
          result => {
            log.debug('token validated');
            this.model.token = token;
            sub.unsubscribe();
          },
          error => {
            sub.unsubscribe();
            return this.router.navigate(['/account']);
          }
        )
      }
    });
  }

  submitReset() {
    if (!this.model.password) {
      return this.errorService.show('Password is required.');
    }

    if (!this.model.confirmPassword) {
      return this.errorService.show('Confirm password is required.');
    }

    if (this.model.password !== this.model.confirmPassword) {
      return this.errorService.show('Passwords must match.');
    }

    if (this.model.password.length < 8) {
      return this.errorService.show('Your password must be 8 or more characters.');
    }

    this.resetWorking = true;
    if (this.user) {
      let sub = this.apiService.updatePassword(this.model.password).subscribe(
        result => {
          sub.unsubscribe();
          this.router.navigate(['/account']);
          this.resetWorking = false;
        },
        error => {
          sub.unsubscribe();
          this.resetWorking = false;
          this.errorService.handle(error);
        }
      );
    } else {
      let sub = this.apiService.changePassword(this.model).subscribe(
        result => {
          sub.unsubscribe();
          this.router.navigate(['/account']);
          this.resetWorking = false;
          const loginModal = this.modalService.open(LoginComponent);
          loginModal.componentInstance.modal = true;
          loginModal.result.then(
            result => {},
            reason => {}
          )
        },
        error => {
          sub.unsubscribe();
          this.resetWorking = false;
          this.errorService.handle(error);
        }
      );
    }
  }

}

export interface ResetModel {
  password: string,
  confirmPassword: string,
  token: string
}
