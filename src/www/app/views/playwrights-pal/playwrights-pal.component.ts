import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'tto-playwrights-pal',
  templateUrl: './playwrights-pal.component.html',
  styleUrls: ['./playwrights-pal.component.less']
})
export class PlaywrightsPalComponent implements OnInit {

  constructor(
    private renderer: Renderer2
  ) { }

  ngOnInit() {
    this.renderer.removeClass(document.body, 'line-paper-30');
  }

}
