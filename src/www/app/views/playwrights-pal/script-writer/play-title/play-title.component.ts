import { Component, OnInit } from '@angular/core';
import { PlaywrightService } from '../../../../services/playwright.service';
import { CacheService } from '../../../../services/cache.service';
import { Subscription } from 'rxjs';
import { Play } from '../../../../common/models';
import { fadeIn } from '../../../../services/animations';

@Component({
  selector: 'tto-play-title',
  templateUrl: './play-title.component.html',
  styleUrls: ['./play-title.component.less', '../script-writer.common.less'],
  animations: [fadeIn]
})
export class PlayTitleComponent implements OnInit {

  readonly activePlay$ = this.playwrightService.activePlay$;
  readonly user$ = this.cacheService.user$;

  activePlay:Play;
  editingTitle:boolean = false;

  _titleEdit:string;
  get titleEdit(): string {
    return this.activePlay && this.activePlay.name ? this.activePlay.name : 'Untitled Play' ;
  }
  set titleEdit(v:string) {
    this._titleEdit = v;
    if (v.length > 0) {
      this.playwrightService.setTitle(v);
    }
  }

  subs:Subscription[] = [];

  constructor(
    private playwrightService: PlaywrightService,
    private cacheService: CacheService
  ) {
    const sub = this.activePlay$.subscribe(play => this.activePlay = play);

    this.subs.push(sub);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  editTitle() {
    this.editingTitle = true;
  }

  doneEditing() {
    this.editingTitle = !this.editingTitle;
    if (this._titleEdit && this._titleEdit.length === 0) {
      this.playwrightService.setTitle(null);
    }
  }

}
