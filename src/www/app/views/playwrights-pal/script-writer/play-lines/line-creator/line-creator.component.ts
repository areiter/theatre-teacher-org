import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { Character, PlayLine, PlayLineType } from '../../../../../common/models';
import { nanoid } from 'nanoid';
import { ReplaySubject, Subscription } from 'rxjs';
import { PlaywrightService } from '../../../../../services/playwright.service';
import { fadeIn } from '../../../../../services/animations';
import { withLatestFrom } from 'rxjs/operators';

@Component({
  selector: 'tto-line-creator',
  templateUrl: './line-creator.component.html',
  styleUrls: ['./line-creator.component.less'],
  animations: [fadeIn]
})
export class LineCreatorComponent implements OnInit {

  @Input() set line(line:PlayLine) {
    this.selectedCharacter = line.character;
    this.newLine.id = line.id;
    this.creatorMode = line.type;
    if (line.type === PlayLineType.DIALOGUE) {
      this.newLine.line = line.text;
    } else if (line.type === PlayLineType.BLOCKING) {
      this.newLine.blocking = line.text;
    }
  }

  @Output() doneEditing:EventEmitter<PlayLine> = new EventEmitter<PlayLine>();

  activePlay$ = this.playwrightService.activePlay$;
  selectedCharacter$ = this.playwrightService.selectedCharacter$;
  newLines$:ReplaySubject<PlayLine> = new ReplaySubject<PlayLine>();

  selectedCharacter:Character;

  creatorMode:PlayLineType = null;
  newLine:any = {
    id: null,
    line: null,
    blocking: null,
    emotion: null
  };

  subs:Subscription[] = [];

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.code ===  'Escape') {
      this.creatorMode = null;

      if (this.newLine.id) {
        this.doneEditing.emit();
      }
    }
  }

  constructor(
    private playwrightService: PlaywrightService
  ) {
    this.subs.push(this.newLines$.pipe(
      withLatestFrom(this.selectedCharacter$)
    ).subscribe(
      ([newLine, selectedCharacter]) => {
        newLine.character = selectedCharacter;
        if (newLine.id) {
          this.playwrightService.editLine(newLine);
          this.doneEditing.emit(newLine);
        } else {
          newLine.id = nanoid(8);
          this.playwrightService.addLine(newLine);
          this.newLine = {
            line: null,
            blocking: null,
            emotion: null
          };
        }
      }
    ));

    this.subs.push(this.selectedCharacter$.subscribe(character => this.selectedCharacter = character));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  isOpen() {
    return this.creatorMode !== null;
  }

  toggleEmotion() {
    this.creatorMode = this.creatorMode === PlayLineType.EMOTION ? null : PlayLineType.EMOTION;
  }

  toggleLine() {
    this.creatorMode = this.creatorMode === PlayLineType.DIALOGUE ? null : PlayLineType.DIALOGUE;
  }

  toggleBlocking() {
    this.creatorMode = this.creatorMode === PlayLineType.BLOCKING ? null : PlayLineType.BLOCKING;
  }

  submitLine() {
    this.newLines$.next({
      id: this.newLine.id,
      type: this.creatorMode,
      text: this.newLine.line
    });
  }

  submitBlocking() {
    this.newLines$.next({
      id: this.newLine.id,
      type: this.creatorMode,
      text: this.newLine.blocking
    });
  }

  blockingShortcut(action) {
    this.newLine.blocking = action;
    this.submitBlocking();
  }

  submitEmotion(emotion) {
    this.newLines$.next({
      id: this.newLine.id,
      type: this.creatorMode,
      text: `feels ${emotion}`
    });
  }

  selectCharacter(character) {
    if (this.selectedCharacter$.getValue() === character) {
      this.selectedCharacter$.next(null);
    } else {
      this.selectedCharacter$.next(character);
    }
  }

}
