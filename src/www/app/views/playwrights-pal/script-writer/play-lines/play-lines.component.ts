import { Component, OnInit, ViewChild } from '@angular/core';
import { Play, PlayLine } from '../../../../common/models';
import { Subscription } from 'rxjs';
import { PlaywrightService } from '../../../../services/playwright.service';
import { CacheService } from '../../../../services/cache.service';
import { fadeIn } from '../../../../services/animations';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { LineCreatorComponent } from './line-creator/line-creator.component';

@Component({
  selector: 'tto-play-lines',
  templateUrl: './play-lines.component.html',
  styleUrls: ['./play-lines.component.less', '../script-writer.common.less'],
  animations: [fadeIn]
})
export class PlayLinesComponent implements OnInit {

  @ViewChild('mainLineCreator', {static: false}) mainLineCreator: LineCreatorComponent;

  readonly activePlay$ = this.playwrightService.activePlay$;
  readonly selectedCharacter$ = this.playwrightService.selectedCharacter$;
  readonly user$ = this.cacheService.user$;

  activePlay:Play;
  editingLine:PlayLine;

  subs:Subscription[] = [];

  constructor(
    private playwrightService: PlaywrightService,
    private cacheService: CacheService
  ) {
    this.subs.push(this.activePlay$.subscribe(play => {
      this.activePlay = play
    }));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  selectCharacter(character) {
    this.selectedCharacter$.next(character);

    if (!this.mainLineCreator.isOpen()) {
      this.mainLineCreator.toggleLine();
    }
  }

  dropLine(event: CdkDragDrop<string[]>) {
    const play = this.activePlay;
    moveItemInArray(play.lines, event.previousIndex, event.currentIndex);
    this.playwrightService.setLines(play.lines);
  }

  editLine(line) {
    this.editingLine = line;
  }

  removeLine(line) {
    this.playwrightService.removeLine(line);
  }

}
