import { Component, OnInit, ViewChild } from '@angular/core';
import { PlaywrightService } from '../../../services/playwright.service';
import { CacheService } from '../../../services/cache.service';
import { User } from '../../../common/models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import { ErrorService } from '../../../services/error.service';

@Component({
  selector: 'tto-script-writer',
  templateUrl: './script-writer.component.html',
  styleUrls: ['./script-writer.component.less']
})
export class ScriptWriterComponent implements OnInit {

  @ViewChild('noAccountModal', null) noAccountModal: NgbModal;

  readonly activePlay$ = this.playwrightService.activePlay$;
  readonly saving$ = this.playwrightService.saving$;
  readonly user$ = this.cacheService.user$;

  user:User;

  constructor(
    private playwrightService: PlaywrightService,
    private cacheService: CacheService,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit() {
    this.user$.subscribe(u => this.user = u);
  }

  save() {
    if (this.user) {
      this.playwrightService.save();
    } else {
      this.modalService.open(this.noAccountModal).result.then(
        () => {
          this.router.navigate(['/account/new']);
        },
        () => {}
      )
    }
  }

  print() {
    this.playwrightService.print();
  }

}
