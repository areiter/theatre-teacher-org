import { Component, OnInit, ViewChild } from '@angular/core';
import { PlaywrightService } from '../../../../services/playwright.service';
import { CacheService } from '../../../../services/cache.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'tto-play-cast',
  templateUrl: './play-cast.component.html',
  styleUrls: ['./play-cast.component.less', '../script-writer.common.less']
})
export class PlayCastComponent implements OnInit {

  @ViewChild('characterEditor', {static: true}) characterEditor: NgbModal;

  readonly activePlay$ = this.playwrightService.activePlay$;
  readonly user$ = this.cacheService.user$;

  edit:any = {
    id: null,
    name: null,
    image: null,
    description: null
  };

  constructor(
    private playwrightService: PlaywrightService,
    private cacheService: CacheService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
  }

  editCharacter(character) {
    this.edit.id = character.id;
    this.edit.name = character.name;
    this.edit.description = character.description;
    this.edit.image = character.image;
    this.modalService.open(this.characterEditor).result.then(
      result => {
        this.playwrightService.updateCharacter({
          ...character,
          name: result.name,
          description: result.description
        });
      },
      reason => {}
    )
  }

}
