import { Component, OnInit } from '@angular/core';
import { PlaywrightService } from '../../../services/playwright.service';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map, withLatestFrom } from 'rxjs/operators';
import { ReplaySubject, Subject } from 'rxjs';

@Component({
  selector: 'tto-play-setup',
  templateUrl: './play-setup.component.html',
  styleUrls: ['./play-setup.component.less']
})
export class PlaySetupComponent implements OnInit {

  readonly activePlay$ = this.playwrightService.activePlay$;
  view$: ReplaySubject<string> = new ReplaySubject<string>();
  removeCharacter$: Subject<number> = new Subject<number>();

  constructor (
    private playwrightService: PlaywrightService,
    private router: Router
  ) {
    this.router.events.pipe(
      filter(evt => evt instanceof NavigationEnd),
      map(evt => (evt as NavigationEnd).url)
    ).subscribe(this.view$);

    this.removeCharacter$.pipe(
      withLatestFrom(this.view$),
      filter(([,view]) => view === '/playwrights-pal/new-play/characters'),
      map(([index,]) => index)
    ).subscribe(index => this.playwrightService.removeCharacter(index as number));
  }

  ngOnInit () {
  }

  removeCharacter (index) {
    this.removeCharacter$.next(index);
  }

}
