import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { PlaywrightService } from '../../../../services/playwright.service';
import moment from 'moment';
import { map } from 'rxjs/internal/operators';
import { Character, Scene } from '../../../../common/models';
import { merge, ReplaySubject } from 'rxjs';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'tto-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.less']
})
export class CharactersComponent implements OnInit {

  readonly activePlay$ = this.playwrightService.activePlay$;
  readonly characterCategories$ = new ReplaySubject<any[]>();

  constructor(
    private playwrightService: PlaywrightService
  ) { }

  ngOnInit() {
    merge(

      this.playwrightService.scenes$.pipe(
        filter(scenes => !!scenes),
        map(scenes => scenes.filter(scene => scene.characters && scene.characters.length > 0)),
        map(scenes => scenes.map(scene => {
          return {
            name: scene.name,
            characters: scene.characters
          }
        }))
      ),

      this.playwrightService.characters$.pipe(
        filter(characters => !!characters && characters.length > 0),
        map(characters => {
          const categories:CharacterCategory[] = [{
            name: null,
            characters: []
          }];
          characters.forEach(char => {
            if (char.category) {
              let category = categories.find(c => c.name === char.category);
              if (!category) {
                category = {
                  name: char.category,
                  characters: []
                };
                categories.push(category);
              }
              category.characters.push(char);
            } else {
              categories[0].characters.push(char);
            }
          });

          return categories;
        })
      )

    ).subscribe(this.characterCategories$);
  }

  selectCharacter(character) {
    this.playwrightService.addCharacter(character);
  }

}

@Pipe({
  name: 'sceneHasCharacters',
  pure: false
})
export class SceneHasCharactersFilter implements PipeTransform {
  transform(items: any[]): any {
    if (items) {
      return items.filter(item => item.characters && item.characters.length > 0);
    } else {
      return items;
    }
  }
}

interface CharacterCategory {
  name: string,
  characters: Character[]
}
