import { Component, OnInit } from '@angular/core';
import { PlaywrightService } from '../../../../services/playwright.service';
import { map } from 'rxjs/internal/operators';
import { Scene } from '../../../../common/models';
import { ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'tto-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.less']
})
export class SettingComponent implements OnInit {

  readonly activePlay$ = this.playwrightService.activePlay$;
  readonly sceneCategories$ = new ReplaySubject<any[]>();

  constructor(
    private playwrightService: PlaywrightService
  ) { }

  ngOnInit() {
    this.playwrightService.scenes$.pipe(
      filter(scenes => !!scenes),
      map(scenes => {
        const categories:SceneCategory[] = [{
          name: null,
          subcategories: [{
            name: null,
            scenes: []
          }]
        }];
        scenes.forEach(s => {
          if (s.category) {
            let category = categories.find(c => c.name === s.category);
            if (!category) {
              category = {
                name: s.category,
                subcategories: [{
                  name: null,
                  scenes: []
                }]
              };
              categories.push(category);
            }

            if (s.subcategory) {
              let subcategory = category.subcategories.find(sc => s.subcategory === sc.name);
              if (!subcategory) {
                subcategory = {
                  name: s.subcategory,
                  scenes: []
                }
                category.subcategories.push(subcategory);
              }
              subcategory.scenes.push(s);
            } else {
              category.subcategories[0].scenes.push(s);
            }
          } else {
            categories[0].subcategories[0].scenes.push(s);
          }
        });

        return categories;
      })
    ).subscribe(this.sceneCategories$);
  }

  selectScene(scene) {
    this.playwrightService.setScene(scene);
  }

}

interface SceneCategory {
  name: string,
  subcategories: [{
    name: string,
    scenes: Scene[]
  }]
}
