import { Component, OnInit } from '@angular/core';
import { PlaywrightService } from '../../../../services/playwright.service';

@Component({
  selector: 'tto-problem',
  templateUrl: './problem.component.html',
  styleUrls: ['./problem.component.less']
})
export class ProblemComponent implements OnInit {

  readonly activePlay$ = this.playwrightService.activePlay$;
  readonly problems$ = this.playwrightService.problems$;

  constructor(
    private playwrightService:PlaywrightService
  ) { }

  ngOnInit() {
  }

  selectProblem(problem) {
    this.playwrightService.setProblem(problem);
  }

  done() {

  }

}
