import { Component, OnInit } from '@angular/core';
import { PlaywrightService } from '../../../services/playwright.service';
import { Lesson } from '../../../common/models';
import { Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'tto-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.less']
})
export class LessonComponent implements OnInit {

  lessons$:ReplaySubject<Lesson[]> = new ReplaySubject<Lesson[]>();

  constructor(
    private playwrightService:PlaywrightService,
    private router:Router
  ) { }

  ngOnInit() {
    this.playwrightService.lessons$.subscribe(this.lessons$);
  }

  selectLesson(lesson?:Lesson) {
    this.playwrightService.setLesson(lesson || null);
  }

}
