import {Component, OnInit, Renderer2, ViewChild} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import { CacheService } from '../../services/cache.service';
import { User } from '../../common/models';

@Component({
  selector: 'tto-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less', '../views.common.less']
})
export class HomeComponent implements OnInit {

  user:User

  constructor(
    private renderer:Renderer2,
    private cacheService:CacheService
  ) { }

  ngOnInit() {
    this.renderer.removeClass(document.body, 'line-paper-30');

    this.cacheService.user$.subscribe(user => this.user = user);
  }

}
