import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tto-teacher-tools',
  templateUrl: './teacher-tools.component.html',
  styleUrls: ['./teacher-tools.component.less', '../views.common.less']
})
export class TeacherToolsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
