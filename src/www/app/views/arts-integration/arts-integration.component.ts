import {Component, OnInit, Renderer2} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'tto-arts-integration',
  templateUrl: './arts-integration.component.html',
  styleUrls: ['./arts-integration.component.less', '../views.common.less']
})
export class ArtsIntegrationComponent implements OnInit {

  readonly lessons = [{
    title: 'Animal Habitat',
    grades: '3rd and 4th grades',
    days: '4 - 5 days',
    duration: '45 - 55 minutes',
    text: 'Students research and explore the five animal categories (bird,fish, mammal, reptile, and amphibian) and their habitats. The lesson culminates with student-written plays that incorporate vocabulary and solutions to changing habitats. ',
    image: 'assets/pictures/arts-integrated-lessons/animal-habitat.webp',
    download: 'https://www.teacherspayteachers.com/Product/Playwriting-with-Animal-Habitats-6396523'
    //download: this.sanitizer.bypassSecurityTrustResourceUrl('assets/downloads/lesson-plans/animal-habitat.pdf')
  }, {
    title: 'The Feudal System',
    grades: '5th and 6th grades',
    days: '4 - 5 days',
    duration: '45 - 55 minutes',
    text: 'This lesson explores the hierarchy of the Feudal System while incorporating Figurative Language and Shakespeare. It culminates with student-written plays that incorporate vocabulary and figurative language.',
    image: 'assets/pictures/arts-integrated-lessons/feudal-system.webp',
    //download: this.sanitizer.bypassSecurityTrustResourceUrl('assets/downloads/lesson-plans/the-feudal-system.pdf')
    download: 'https://www.teacherspayteachers.com/Product/Feudal-System-Researching-to-Performance-6468106'
  }, {
    title: 'Revolutionary War',
    grades: '3rd and 4th grades',
    days: '4 - 5 days',
    duration: '45 - 55 minutes',
    text: 'This lesson explores the cuases of the Revolutionary War and introduces the elements of storytelling and playwrighting. It culminates with student-written plays that incorporate vocabulary and historical characters from the Revolutionary War',
    image: 'assets/pictures/arts-integrated-lessons/revolutionary-war.webp',
    //download: this.sanitizer.bypassSecurityTrustResourceUrl('assets/downloads/lesson-plans/revolutionary-war.pdf')
    download: 'https://www.teacherspayteachers.com/Product/Causes-of-the-Revolutionary-War-Learning-to-Performance-6468132'
  }];

  constructor(
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }

}
