import {Component, OnInit, Renderer2} from '@angular/core';

@Component({
  selector: 'tto-at-home',
  templateUrl: './at-home.component.html',
  styleUrls: ['./at-home.component.less', '../views.common.less']
})
export class AtHomeComponent implements OnInit {

  constructor(
    private renderer:Renderer2
  ) { }

  ngOnInit() {
    //this.renderer.addClass(document.body, 'line-paper-30');
  }

}
