import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'tto-performances',
  templateUrl: './performances.component.html',
  styleUrls: ['../views.common.less', './performances.component.less']
})
export class PerformancesComponent implements OnInit {

  readonly performances = [{
    title: 'Fairy Tales in the Time of Covid',
    text: 'Our favorite characters from the land of stories, including Red Riding Hood, The Three Little Pigs, and Billy Goats Gruff try to navigate and survive the world of Zoom and Covid. Includes Teacher Guide with in-classroom activities that will have students creating and performing.',
    length: '40 minutes',
    cost: '$50 per school, or $25 per family',
    buy: 'mailto:teralyn@theatreteacher.org?subject=Purchase Fairy Tales in the Time of Covid',
    image: 'assets/pictures/performance/fairytales-covid.png'
  }];

  readonly historicalPerformances = [{
    title: 'Margaret Hamilton',
    quote: 'I was attracted by the sheer idea and the fact it had never been done before.',
    text: 'Margaret Heafield Hamilton is an American computer scientist and systems engineer. She was director of the Software Engineering Division of the MIT Instrumentation Laboratory, which developed on-board flight software for NASA’s Apollo program. She later founded two software companies—Higher Order Software in 1976 and Hamilton Technologies in 1986, both in Cambridge, Massachusetts.',
    image: 'assets/pictures/performance/margaret-hamilton.webp',
    download: 'assets/downloads/lesson-plans/margaret-hamilton.pdf',
    video: this.sanitizer.bypassSecurityTrustResourceUrl('https://player.vimeo.com/video/500503973')
  }, {
    title: 'Robert Smalls',
    quote: 'Let us make a constitution for all the people, one we will be proud of and our children will receive with delight.',
    text: 'Robert Smalls was an American politician, publisher and naval pilot. Born into slavery in Beaufort, South Carolina, he freed himself, his crew, and their families during the American Civil War by commandeering a Confederate transport ship, CSS Planter, in Charleston harbor, on May 13, 1862, and sailing it from Confederate-controlled waters of the harbor to the U.S. blockade that surrounded it. He then piloted the ship to the Union-controlled enclave in Beaufort-Port Royal-Hilton Head area, where it became a Union warship. His example and persuasion helped convince President Abraham Lincoln to accept African-American soldiers into the Union Army.',
    image: 'assets/pictures/performance/robert-smalls.webp',
    download: 'assets/downloads/lesson-plans/robert-smalls.pdf',
    video: this.sanitizer.bypassSecurityTrustResourceUrl('https://player.vimeo.com/video/500513681')
  }, {
    title: 'Stanislav Petrov',
    quote: 'When we deal with space, when we play god, who knows what will be the next surprise.',
    text: 'Stanislav Yevgrafovich Petrov was a lieutenant colonel of the Soviet Air Defence Forces who played a key role in the 1983 Soviet nuclear false alarm incident. On 26 September 1983, three weeks after the Soviet military had shot down Korean Air Lines Flight 007, Petrov was the duty officer at the command center for the Oko nuclear early-warning system when the system reported that a missile had been launched from the United States, followed by up to five more. Petrov judged the reports to be a false alarm, and his decision to disobey orders, against Soviet military protocol, is credited with having prevented an erroneous retaliatory nuclear attack on the United States and its NATO allies that could have resulted in a large-scale nuclear war. Investigation later confirmed that the Soviet satellite warning system had indeed malfunctioned.',
    image: 'assets/pictures/performance/stanislav-petrov.webp',
    download: 'assets/downloads/lesson-plans/stanislav-petrov.pdf',
    video: this.sanitizer.bypassSecurityTrustResourceUrl('https://player.vimeo.com/video/500511616')
  }, {
    title: 'Emily Geiger',
    quote: 'I was alone in the room. I took the note out. I read it. Over and over and over. I memorized every word, every comma, everything.',
    text: 'Emily Geiger is an American Revolutionary War heroine who was captured by the British while on a military mission as a civilian. She was carrying an important message across enemy grounds when she was captured and questioned. The Tory matron could find nothing on her because Geiger had memorized the message, then eaten it, so they had to let her go. She proceeded, reached the general she had sought, and verbally delivered the message.',
    image: 'assets/pictures/performance/emily-geiger.webp',
    download: 'assets/downloads/lesson-plans/emily-geiger.pdf',
    video: this.sanitizer.bypassSecurityTrustResourceUrl('https://player.vimeo.com/video/502711405')
  }];

  constructor(
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }

}
