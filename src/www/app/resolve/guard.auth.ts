import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { CacheService } from '../services/cache.service';

@Injectable()
export class UserAuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private cacheService: CacheService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.cacheService.getUser() && this.cacheService.getAccessToken()) {
      return true;
    } else {
      this.router.navigate(['/account/new']);
      return false;
    }
  }
}
