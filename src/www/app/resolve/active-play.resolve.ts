import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { PlaywrightService } from '../services/playwright.service';
import { Play } from '../common/models';

@Injectable()
export class ActivePlayResolve implements Resolve<any> {

  constructor(
    private playwrightService:PlaywrightService
  ) {}

  resolve(route: ActivatedRouteSnapshot):Play {
    const activePlay = this.playwrightService.activePlay$.getValue();
    if (!activePlay) {
      this.playwrightService.startNewCache();
    }
    return activePlay;
  }
}
