import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { CacheService } from '../services/cache.service';

@Injectable()
export class ActivePlayGuard implements CanActivate {

  constructor(
    private router: Router,
    private cacheService: CacheService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean {
    const cachedPlay = this.cacheService.getCachedPlay();
    if (cachedPlay
      && cachedPlay.scene
      && cachedPlay.characters
      && cachedPlay.characters.length > 0
      && cachedPlay.problem
    ) {
      return true;
    } else {
      this.router.navigate(['/playwrights-pay/new-play']);
      return false;
    }
  }
}
