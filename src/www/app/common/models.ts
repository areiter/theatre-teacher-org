export interface NewUser {
  username: string,
  email: string,
  password: string,
  firstName: string,
  lastName: string,
  teacher: boolean
}

export interface User {
  id: string,
  username: string,
  email: string,
  firstName: string,
  lastName: string,
  teacher: boolean,
  lastLogin: Date,
  createdDate: Date
}

export interface Lesson {
  id: string,
  name: string
}

export interface Scene {
  id: string,
  name: string,
  image: string,
  description: string,
  category?: string,
  subcategory?: string,
  default: boolean,
  characters?: Character[],
  user?: User,
  lesson?: string
}

export interface Character {
  id: string,
  templateId: string,
  name: string,
  image: string,
  description: string,
  category?: string,
  scene?: Scene,
  lesson?: string,
}

export interface Problem {
  id: string,
  name: string,
  image: string,
  description: string
}

export interface PlayLine {
  id?: string,
  character?: Character,
  text: string,
  type: PlayLineType
}

export enum PlayLineType {
  DIALOGUE = 'dialogue',
  BLOCKING = 'blocking',
  EMOTION = 'emotion',
}

export interface Play {
  id: string,
  derivedId: string,
  user: User,
  sharedWith: User[],
  name: string,
  lesson?: string,
  scene: Scene,
  characters: Character[],
  problem: Problem,
  lines: PlayLine[],
  state: PlayState,
  deleting?: boolean
}

export enum PlayState {
  IN_PROGRESS = 'in-progress',
  COMPLETE = 'complete',
  PUBLISHED = 'published'
}

export class ServerPlay {
  id: string;
  name: string;
  derivedId: string;
  scene: {
    template: string, //{type: mongoose.Schema.Types.ObjectId, ref: 'Scene'},
    name: string,
    description: string
  };
  characters: {
    template: string, //{type: mongoose.Schema.Types.ObjectId, ref: 'Character'},
    name: string,
    description: string
  }[];
  problem: {
    template: string, //{type: mongoose.Schema.Types.ObjectId, ref: 'Problem'},
    name: string,
    description: string,
  };
  user: string; //{type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true},
  sharedWith: string[]; //[{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  state: string; //{type: String, enum: ['in-progress', 'complete', 'published']},
  lines: {
    type: string,
    text: string,
    characterIndex: number
  }[];

  constructor (play?: Play, author?: User) {
    if (author) {
      this.user = author.id;
    }

    if (play) {
      this.id = play.id.length === 8 ? null : play.id;
      this.name = play.name;
      this.sharedWith = play.sharedWith.map(u => u.id);
      this.state = play.state;
      this.scene = {
        template: play.scene.id,
        name: play.scene.name,
        description: play.scene.description
      };
      this.problem = {
        template: play.problem.id,
        name: play.problem.name,
        description: play.problem.description
      };
      this.characters = play.characters.map(c => {
        return {
          template: c.templateId,
          name: c.name,
          description: c.description
        }
      });
      this.lines = play.lines.map(l => {
        return {
          type: l.type,
          text: l.text,
          characterIndex: this.characters.findIndex(c => c.template === l.character.templateId)
        }
      });
    }
  }
}
