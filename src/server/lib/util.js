const log   = require('./log').create('lib/util'),
  _         = require('underscore'),
  dot       = require('dot-object'),
  httpError = require('http-errors');

exports.mapParams = (params, req, res) => {
  let optional = false;
  const result = {}, reqRes = {
    req: req,
    res: res
  };
  for (let key in params) {
    if (params.hasOwnProperty(key)) {
      let value = params[key];
      if (key.substring(key.length - 1) === '?') {
        optional = true;
        key = key.substring(0, key.length - 1);
      }
      const p = dot.pick(value, reqRes);
      if (p !== undefined && p !== null) {
        result[key] = p;
      } else {
        let expected = value;
        if (Array.isArray(value)) {
          expected = value.join(' or ');
        }
        if (optional) {
          //log.info('Couldn\'t find ' + expected + ' in in ' + req.method + ' on ' + req.originalUrl + ', but ' + (_.isArray(value) && value.length > 1 ? 'they are' : 'it is') + ' marked as optional');
        } else {
          log.error('Couldn\'t find ' + expected + ' in ' + req.method + ' on ' + req.originalUrl);
          throw httpError(400, {message: 'Couldn\'t find ' + expected + ' in ' + req.method + ' on ' + req.originalUrl, code: 4001});
        }
      }
    }
  }
  return result;
};

exports.handleErrors = (err, req, res, cont) => {
  if ((!err.status && !err.statusCode) || err.status === 500 || err.statusCode === 500) {
    if (err.stack) {
      log.error(err.stack);
    } else {
      log.error(err);
    }
  }

  let body;
  if (err.number && err.message) {
    body = {
      "error_code": err.number,
      "error_description": err.message
    }
  } else if (err.status === 401 || err.statusCode === 401) {
    body = {
      "error_code": '4010',
      "error_description": "Application does not have permission"
    };
  } else if (err.status && err.message) {
    body = {
      "error_code": '' + err.status + '0',
      "error_description": err.message || "An unknown error occurred"
    };
  } else {
    body = {
      "error_code": '5000',
      "error_description": "An unknown error occurred"
    };
  }
  res.set('errordescription', body.error_description);
  res.set('errorcode', body.error_code);

  res.status(err.status || err.statusCode || 500).send(body);
};
