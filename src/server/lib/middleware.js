const httpError = require('http-errors');
const jwt = require('jsonwebtoken');
const db = require('./db');
const log = require('./log').create('middleware');

const secret = process.env.secret;

module.exports.protect = async (req, res, cont) => {
  try {
    if (!secret) {
      return cont(httpError(500, {message: 'No JWT secret configured, cannot authenticate!', number: 5004}));
    }

    const authHeader = req.headers.authorization;
    if (!authHeader) {
      log.warn('No auth header found in request');
      return cont(httpError(401, {message: 'No authorization header found in request', number: 4016}));
    }

    let accessToken = authHeader.split(' ');
    if (!accessToken || accessToken.length !== 2) {
      log.warn('Unable to extract auth token from request header', authHeader);
      return cont(httpError(401, {message: 'Unable to extract auth token from header', number: 4016}));
    }

    req.decoded = await new Promise((resolve, reject) => {
      jwt.verify(accessToken[1], secret, (err, decoded) => {
        if (err) {
          if (err.name === 'TokenExpiredError') {
            log.warn('Token refresh required!');
            reject(httpError(403, {message: 'Token refresh required', number: 4010}));
          } else {
            log.warn('Invalid token:', err.name, err.message);
            reject(httpError(401, {message: 'Bad access token!', number: 4011}));
          }
        } else {
          resolve(decoded);
        }
      })
    });

    const session = await db.Session.findById(req.decoded.session).populate('user');
    if (!session) {
      log.warn('Invalid token: session has expired!');
      return cont(httpError(401, {message: 'Session has expired!', number: 4011}));
    }

    req.session = session;

    cont();
  } catch (err) {
    cont(err);
  }
};

module.exports.sendJSONResponse = (req, res) => {
  res.type('json');
  res.status(200).send(res.body);
};

module.exports.sendSimpleSuccess = (req, res) => {
  res.type('json');
  res.status(200).send({
    success: true
  });
};
