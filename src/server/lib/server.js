const server = exports,

  fs          = require('fs'),
  express     = require('express'),
  bodyParser  = require('body-parser'),
  path        = require('path'),
  morgan      = require('morgan'),
  hsts        = require('hsts'),
  util        = require('./util'),
  log         = require('./log').create('lib/server');

const ssrAvailable = fs.existsSync(path.join(__dirname, '../ssr/main.js'));

server.start = () => {
  const app = express();

  app.use(require('./log').requestId);

  // Log http requests
  app.use(morgan('common'));

  // parse application/x-www-form-urlencoded
  // parse application/json
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

  // Enforce strict transport security
  app.use((req, res, next) => {
    if (req.secure || (req.headers['x-forwarded-proto'] === 'https')) {
      hsts({maxAge: 1234000})(req, res, next);
    } else {
      if (process.env.NODE_ENV === 'local') {
        next();
      } else {
        res.redirect('https://' + req.headers.host + req.url);
      }
    }
  });

  // Disable powered by express header
  app.disable('x-powered-by');

  // Bind routers
  app.use('/api/authentication', require('../routers/authentication'));
  app.use('/api/user', require('../routers/user'));
  app.use('/api/play', require('../routers/play'));

  app.get('/api/test', (req, res) => {
    res.send('Web app server running');
  });

  // Server-side rendering
  if (ssrAvailable) {
    log.info('Server-side rendering enabled.');
    require('zone.js/dist/zone-node');
    const {provideModuleMap} = require('@nguniversal/module-map-ngfactory-loader');
    const {SSRServerModuleNgFactory, LAZY_MODULE_MAP} = require('../ssr/main');
    const ngExpressEngine = require('@nguniversal/express-engine').ngExpressEngine;
    const domino = require('domino');

    const template = fs.readFileSync(path.resolve(__dirname, '../public/index.html')).toString();
    const win = domino.createWindow(template);
    global['window'] = win;
    global['document'] = win.document;

    // Setup SSR engine
    app.engine('html', ngExpressEngine({
      bootstrap: SSRServerModuleNgFactory,
      providers: [
        provideModuleMap(LAZY_MODULE_MAP)
      ]
    }));

    app.set('view engine', 'html');
    app.set('views', path.resolve(__dirname, '../public'));

    app.get('*.*', express.static(path.resolve(__dirname, '../public')));

    app.get('*', (req, res) => {
      res.render('index', {req});
    });
  } else {
    log.warn('No SSR entry point detected, server-side rendering is disabled.');
  }

  app.use(express.static(path.resolve(__dirname, '../public')));
  app.get('/*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../public/index.html'));
  });

  app.use(util.handleErrors);

  return app;
};
