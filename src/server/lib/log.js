const log = exports,

  nanoid     = require('nanoid'),
  colors      = require('colors'),
  httpContext = require('express-http-context'),
  path        = require('path'),
  winston     = require('winston');

log.requestId = [
  httpContext.middleware,
  (req, res, cont) => {
    const id = nanoid.nanoid(6);
    httpContext.set('reqId', id);
    res.set('X-Request-Id', id);
    cont();
  }
];

// if (process.env.NODE_ENV !== 'test') {
//   const crashLogger = new (winston.Logger)({
//     transports: [
//       // Log to console
//       new (winston.transports.Console)({
//         label: 'crash',
//         timestamp: true,
//         colorize: true,
//         level: 'error',
//         handleExceptions: true,
//         humanReadableUnhandledException: true
//       }),
//
//       // Log to a file
//       new (winston.transports.File)({
//         filename: path.join(__dirname, '../../../app-crash.log'),
//         tailable: true,
//         handleExceptions: true,
//         humanReadableUnhandledException: true,
//         json: false,
//         level: 'error'
//       })
//     ]
//   });
//
//   crashLogger.exitOnError = true;
// }

log.formatter = function (message) {
  const id = httpContext.get('reqId');
  return (id ? colors.blue('(' + id + ') ') : '') + message;
};

log.timestamp = function () {
  return new Date().toISOString()
};

log.create = (name) => {
  const logger = new (winston.Logger)({
    transports: [

      // Log to console
      new (winston.transports.Console)({
        colorize: true,
        label: name,
        timestamp: true,
        level: 'debug'}),

      // Log to a file
      // new (winston.transports.File)({
      //   label: name,
      //   filename: path.join(__dirname, '../../../app.log'),
      //   maxsize: 10 * 1048576, // once a log file hits 100 mb a new file is created
      //   zippedArchive: true, // zip all but latest
      //   tailable: true, // latest file will always be 'app.log'
      //   maxFiles: 10,
      //   //json: false,
      //   level: 'info'
      // })
    ]
  });

  return {
    log: function (level, message) {
      logger.log(level, log.formatter(message));
    },
    error: function (message) {
      logger.error(log.formatter(message));
    },
    warn: function (message) {
      logger.warn(log.formatter(message));
    },
    verbose: function (message) {
      logger.verbose(log.formatter(message));
    },
    info: function (message) {
      logger.info(log.formatter(message));
    },
    debug: function (message) {
      logger.debug(log.formatter(message));
    },
    silly: function (message) {
      logger.silly(log.formatter(message));
    }
  }
};
