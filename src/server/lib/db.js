const mongoose = require('mongoose');
const log = require('./log').create('db');

const con = {
  user: process.env.mongo_user,
  password: process.env.mongo_password,
  cluster: process.env.mongo_cluster,
  db: process.env.mongo_db
}

const connectionString = `mongodb+srv://${con.user}:${con.password}@${con.cluster}/${con.db}?retryWrites=true&w=majority`;

module.exports.connect = async () => {
  return new Promise((resolve, reject) => {
    mongoose.Promise = global.Promise;
    mongoose.set('useCreateIndex', true);
    mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});
    const db = mongoose.connection;
    let resolved = false;
    db.on('error', (err) => {
      log.error(err);
      if (!resolved) {
        reject(err);
      }
    });
    db.once('open', () => {
      log.info('Mongodb connected, creating models...');
      resolved = true;
      createModels(db);
      log.info('Models created, database connection ready.');
      resolve(db);
    });
  });
};

const createModels = (db) => {
  // User
  const userSchema = new mongoose.Schema({
    username: {type: String, required: true, index: true, unique: true},
    email: {type: String, unique: true},
    firstName: String,
    lastName: String,
    password: {type: String, required: true},
    teacher: {type: Boolean, default: false},
    lastLogin: Date,
    createdDate: {type: Date, default: Date.now}
  }, {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  });

  userSchema.methods.sanitized = function () {
    const sanitized = ({password, ...rest}) => rest;
    return sanitized(this.toObject());
  }

  module.exports.User = db.model('User', userSchema);

  // Session
  const sessionSchema = new mongoose.Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    refreshToken: {type: String, required: true, index: true},
    expires: {type: Date},
    createdDate: {type: Date, default: Date.now}
  }, {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  });

  module.exports.Session = db.model('Session', sessionSchema);

  // Playwright's pal models
  // Lesson
  const lessonSchema = new mongoose.Schema({
    name: {type: String, required: true},
    image: {type: String},
    description: String,
    createdDate: {type: Date, default: Date.now}
  }, {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  });

  module.exports.Lesson = db.model('Lesson', lessonSchema);

  // Scene
  const sceneSchema = new mongoose.Schema({
    name: {type: String, required: true},
    category: {type: String, required: false, index: true},
    subcategory: {type: String, required: false, index: true},
    image: {type: String, required: true},
    description: String,
    shared: {type: Boolean, default: false},
    lesson: {type: mongoose.Schema.Types.ObjectId, ref: 'Lesson'},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    createdDate: {type: Date, default: Date.now}
  }, {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  });

  sceneSchema.virtual('characters', {
    ref: 'Character',
    localField: '_id',
    foreignField: 'scene'
  });

  module.exports.Scene = db.model('Scene', sceneSchema);

  // Character
  const characterSchema = new mongoose.Schema({
    name: {type: String, required: true},
    image: {type: String, required: true},
    category: {type: String, required: false, index: true},
    subcategory: {type: String, required: false, index: true},
    description: String,
    scene: {type: mongoose.Schema.Types.ObjectId, ref: 'Scene'},
    lesson: {type: mongoose.Schema.Types.ObjectId, ref: 'Lesson'},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    createdDate: {type: Date, default: Date.now}
  }, {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  });

  module.exports.Character = db.model('Character', characterSchema);

  // Character
  const problemSchema = new mongoose.Schema({
    name: {type: String, required: true},
    image: {type: String, required: true},
    description: String,
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    lesson: {type: mongoose.Schema.Types.ObjectId, ref: 'Lesson'},
    createdDate: {type: Date, default: Date.now}
  }, {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  });

  module.exports.Problem = db.model('Problem', problemSchema);

  // Play
  const playSchema = new mongoose.Schema({
    name: {type: String, required: true},
    derivedId: String,
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true},
    sharedWith: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    state: {type: String, enum: ['in-progress', 'complete', 'published']},
    lesson: {type: mongoose.Schema.Types.ObjectId, ref: 'Lesson'},
    scene: {
      template: {type: mongoose.Schema.Types.ObjectId, ref: 'Scene'},
      name: {type: String},
      description: String
    },
    characters: [{
      template: {type: mongoose.Schema.Types.ObjectId, ref: 'Character'},
      name: {type: String},
      description: String
    }],
    problem: {
      template: {type: mongoose.Schema.Types.ObjectId, ref: 'Problem'},
      name: {type: String},
      description: {type: String},
    },
    lines: [{
      type: {type: String, enum: ['dialogue', 'blocking', 'emotion']},
      text: {type: String},
      characterIndex: {type: Number}
    }],
    createdDate: {type: Date, default: Date.now}
  }, {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  });

  module.exports.Play = db.model('Play', playSchema);

  // Temp data
  const tempSchema = new mongoose.Schema({
    key: {type: String, index: true},
    value: {type: String},
    createdDate: {type: Date, default: Date.now}
  }, {
    toObject: {
      virtuals: true
    },
    toJSON: {
      virtuals: true
    }
  });

  module.exports.Temp = db.model('Temp', tempSchema);
};
