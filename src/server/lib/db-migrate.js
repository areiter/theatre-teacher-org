const db = require('./db');
const log = require('./log').create('db-migrate');

const defaultLessons = [{
  name: 'Feudal System',
  description: ''
}, {
  name: 'Animal Habitat',
  description: ''
}, {
  name: 'Revolutionary War',
  description: ''
}];

const defaultScenes = [{
  name: 'Deep Dark Cave',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/deep_dark_cave_bg.png',
  shared: true
}, {
  name: 'Magical Forest',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/magical_forest_bg.png',
  shared: true
}, {
  name: 'Under the Ocean',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/ocean_background.png',
  shared: true
}, {
  name: 'Robot World',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/robot_world_bg.png',
  shared: true
}, {
  name: 'Outer Space',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/space-bg.png',
  shared: true
}, {
  name: 'Create Your Own',
  description: '',
  image: '',
  shared: true
}, {
  name: 'Town',
  category: 'Exterior',
  subcategory: 'After Bubonic Plague',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/feudal-post-ext1-bg.png',
  lesson: 'Feudal System'
}, {
  name: 'Plague Throne Room',
  category: 'Interior',
  subcategory: 'After Bubonic Plague',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/feudal-post-int1-bg.png',
  lesson: 'Feudal System'
}, {
  name: 'Dungeon',
  category: 'Interior',
  subcategory: 'After Bubonic Plague',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/feudal-post-int2-bg.png',
  lesson: 'Feudal System'
}, {
  name: 'Castle Gate',
  category: 'Exterior',
  subcategory: 'Before Bubonic Plague',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/feudal-post-ext1-bg.png',
  lesson: 'Feudal System'
}, {
  name: 'Kingdom',
  category: 'Exterior',
  subcategory: 'Before Bubonic Plague',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/feudal-post-ext2-bg.png',
  lesson: 'Feudal System'
}, {
  name: 'Great Hall',
  category: 'Interior',
  subcategory: 'Before Bubonic Plague',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/feudal-pre-int1-bg.png',
  lesson: 'Feudal System'
}, {
  name: 'Throne Room',
  category: 'Interior',
  subcategory: 'Before Bubonic Plague',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/feudal-pre-int2-bg.png',
  lesson: 'Feudal System'
}, {
  name: 'Jungle Habitat',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/animal-jungle-bg.png',
  lesson: 'Animal Habitat'
}, {
  name: 'Meadow Habitat',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/animal-land-bg.png',
  lesson: 'Animal Habitat'
}, {
  name: 'Ocean Habitat',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/animal-ocean-bg.png',
  lesson: 'Animal Habitat'
}, {
  name: 'Pond Habitat',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/animal-pond-bg.png',
  lesson: 'Animal Habitat'
}, {
  name: 'Trees Habitat',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/animal-trees-bg.png',
  lesson: 'Animal Habitat'
}, {
  name: 'Patriot House',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/revolutionary-home-1.png',
  lesson: 'Revolutionary War'
}, {
  name: 'Patriot Cabin',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/revolutionary-home-2.png',
  lesson: 'Revolutionary War'
}, {
  name: 'Patriot City',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/revolutionary-street-1.png',
  lesson: 'Revolutionary War'
}, {
  name: 'Patriot Town',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/backgrounds/revolutionary-street-2.png',
  lesson: 'Revolutionary War'
}];

const defaultCharacters = [{
  scene: 'Deep Dark Cave',
  characters: [{
    name: 'Cave Guardian',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/deep_dark_cave_gaurdian.png'
  }, {
    name: 'Cave Kraken',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/deep_dark_cave_kraken.png'
  }, {
    name: 'Cave Monster',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/deep_dark_cave_monster.png'
  }, {
    name: 'Cave Robot',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/deep_dark_cave_robot.png'
  }, {
    name: 'Cave Snake',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/deep_dark_cave_snake.png'
  }, {
    name: 'Stan',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/deep_dark_cave_stan.png'
  }, {
    name: 'Stella',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/deep_dark_cave_stella.png'
  }, {
    name: 'Tim',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/deep_dark_cave_tim.png'
  }, {
    name: 'Tina',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/deep_dark_cave_tina.png'
  }]
}, {
  scene: 'Magical Forest',
  characters: [{
    name: 'Forest Dragon',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_dragon.png'
  }, {
    name: 'Elf Kid',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_elfkid.png'
  }, {
    name: 'Elf King',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_elfking.png'
  }, {
    name: 'Elf Queen',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_elfqueen.png'
  }, {
    name: 'Dark Fairy',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_evilfairy.png'
  }, {
    name: 'Dark Fairy King',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_evilfairyking.png'
  }, {
    name: 'Dark Fairy Queen',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_evilfairyqueen.png'
  }, {
    name: 'Witch',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_evilwitch.png'
  }, {
    name: 'Fairy',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_fairy.png'
  }, {
    name: 'Fairy Queen',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_fairyqueen.png'
  }, {
    name: 'Gnome',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_gnome.png'
  }, {
    name: 'Good Witch',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_goodwitch.png'
  }, {
    name: 'Tree Monster',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_treemonster.png'
  }, {
    name: 'Tree Spirit',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_treespirit.png'
  }, {
    name: 'Wizard',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/magical_forest_wizard.png'
  }]
}, {
  scene: 'Under the Ocean',
  characters: [{
    name: 'Jellyfish',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/ocean_jellyfish.png'
  }, {
    name: 'Ocean King',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/ocean_king_poseidon.png'
  }, {
    name: 'Ocean Queen',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/ocean_sea_queen.png'
  }, {
    name: 'Mermaid',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/ocean_mermaid.png'
  }, {
    name: 'Merman',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/ocean_merman.png'
  }, {
    name: 'Octopus',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/ocean_octopus.png'
  }, {
    name: 'Sea Witch',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/ocean_sea_witch.png'
  }, {
    name: 'Swordfish Warrior',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/ocean_swordfish_warrior.png'
  }]
}, {
  scene: 'Robot World',
  characters: [{
    name: 'Robot Baker',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/robot_baker.png'
  }, {
    name: 'Robot Chef',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/robot_chef.png'
  }, {
    name: 'Robot Dark Scientist',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/robot_evil_scientist.png'
  }, {
    name: 'Robot Experiment',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/robot_experiment.png'
  }, {
    name: 'Robot Friend',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/robot_friend.png'
  }, {
    name: 'Robot Mechanic',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/robot_mechanic.png'
  }, {
    name: 'Robot Warrior',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/robot_warrior.png'
  }]
}, {
  scene: 'Outer Space',
  characters: [{
    name: 'Dark Alien King',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/space-evil-king.png'
  }, {
    name: 'Dark Alien Warrior',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/space-evil-warrior.png'
  }, {
    name: 'Alien Queen',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/space-queen.png'
  }, {
    name: 'Space Robot',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/space-robot.png'
  }, {
    name: 'Happy Star',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/space-star.png'
  }, {
    name: 'Alien Warrior',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/space-warrior.png'
  }, {
    name: 'Alien Witch',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/space-witch.png'
  }]
}, {
  scene: 'Create Your Own',
  characters: [{
    name: 'Custom Character 1',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/create_your_own_character-blue.png'
  }, {
    name: 'Custom Character 2',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/create_your_own_character-green.png'
  }, {
    name: 'Custom Character 3',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/create_your_own_character-orange.png'
  }, {
    name: 'Custom Character 4',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/create_your_own_character-red.png'
  }, {
    name: 'Custom Character 5',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/create_your_own_character-yellow.png'
  }, {
    name: 'Custom Character 6',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/create_your_own_character-pink.png'
  }]
}, {
  lesson: 'Feudal System',
  characters: [{
    name: 'King',
    description: '',
    category: 'Kings & Queens',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/feudal_king.png'
  }, {
    name: 'Queen',
    description: '',
    category: 'Kings & Queens',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/feudal_queen.png'
  }, {
    name: 'Male Noble',
    description: '',
    category: 'Nobles',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/feudal_noble1.png'
  }, {
    name: 'Female Noble',
    description: '',
    category: 'Nobles',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/feudal_noble2.png'
  }, {
    name: 'Knight',
    description: '',
    category: 'Knights & Warriors',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/feudal_knight.png'
  }, {
    name: 'Male Peasant',
    description: '',
    category: 'Peasants',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/feudal_peasant1.png'
  }, {
    name: 'Female Peasant',
    description: '',
    category: 'Peasants',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/feudal_peasant2.png'
  }]
}, {
  lesson: 'Animal Habitat',
  characters: [{
    name: 'Bird',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/animal-bird.png'
  }, {
    name: 'Bunny',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/animal-bunny.png'
  }, {
    name: 'Duck',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/animal-duck.png'
  }, {
    name: 'Fish',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/animal-fish.png'
  }, {
    name: 'Frog',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/animal-frog.png'
  }, {
    name: 'Snake',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/animal-snake.png'
  }]
}, {
  lesson: 'Revolutionary War',
  characters: [{
    name: 'Benedict Arnold',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-benedict-arnold.png'
  }, {
    name: 'British Soldier',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-british-soldier.png'
  }, {
    name: 'Charles Cornwallis',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-charles-cornwallis.png'
  }, {
    name: 'Colonist',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-colonist.png'
  }, {
    name: 'Crispus Attucks',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-crispus-attucks.png'
  }, {
    name: 'George Washington',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-george-washington.png'
  }, {
    name: 'King George III',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-king-george-iii.png'
  }, {
    name: 'Loyalist',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-loyalist.png'
  }, {
    name: 'Nathan Hale',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-nathan-hale.png'
  }, {
    name: 'Patrick Henry',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-patrick-henry.png'
  }, {
    name: 'Patriot',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-patriot.png'
  }, {
    name: 'Salem Poor',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-salem-poor.png'
  }, {
    name: 'Thomas Cage',
    description: '',
    image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/characters/revolutionary-thomas-cage.png'
  }]
}];

const defaultProblems = [{
  name: 'Object',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_1.png'
}, {
  name: 'Disaster',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_2.png'
}, {
  name: 'Friends',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_3.png'
}, {
  name: 'Stranger',
  description: '',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_4.png'
}, {
  name: 'Changing Habitat',
  description: '',
  lesson: 'Animal Habitat',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_animal1.png'
}, {
  name: 'My Own Idea',
  description: '',
  lesson: 'Animal Habitat',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_animal2.png'
}, {
  name: 'Everyone is Dying',
  description: '',
  lesson: 'Feudal System',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_feudal1.png'
}, {
  name: 'Peasant Revolt',
  description: '',
  lesson: 'Feudal System',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_feudal2.png'
}, {
  name: 'My Own Idea',
  description: '',
  lesson: 'Feudal System',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_feudal3.png'
}, {
  name: 'My Own Idea',
  description: '',
  lesson: 'Revolutionary War',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_revolutionary1.png'
}, {
  name: 'Patriots Stand Up',
  description: '',
  lesson: 'Revolutionary War',
  image: 'https://tto-assets.s3.amazonaws.com/playwrights-pal/problems/problem_revolutionary2.png'
}];

module.exports = async () => {
  // Lessons
  let ps = [];
  log.info('Check lessons');
  const lessonCount = await db.Lesson.countDocuments({});
  if (lessonCount === 0) {
    log.info('No Lessons found in database, adding defaults...');
    defaultLessons.forEach(lesson => ps.push(new db.Lesson(lesson).save()));
  } else {
    for (const defaultLesson of defaultLessons) {
      const exists = await db.Lesson.findOne({name: defaultLesson.name});
      if (!exists) {
        log.info('Adding ' + defaultLesson.name + ' lesson to DB.');
        ps.push(new db.Lesson(defaultLesson).save());
      }
    }
  }

  if (ps.length > 0) {
    try {
      await Promise.all(ps);
      log.info('Added ' + ps.length + ' default Lessons to database.');
    } catch (err) {
      log.error('There was an error adding default Lessons!');
      log.error(err);
    }
  }

  // Scenes
  log.info('Check scenes');
  for (const defaultScene of defaultScenes) {
    let exists, lesson;
    if (defaultScene.lesson) {
      lesson = await db.Lesson.findOne({name: defaultScene.lesson}, {id: 1});
      if (!lesson) {
        log.error('Scene ' + defaultScene.name + ' has an invalid lesson name: ' + defaultScene.lesson);
        continue;
      }

      exists = await db.Scene.findOne({name: defaultScene.name, lesson: lesson.id});
    } else {
      exists = await db.Scene.findOne({name: defaultScene.name, lesson: null});
    }
    if (!exists) {
      log.info('Adding ' + defaultScene.name + ' scene to DB.');
      if (defaultScene.lesson) {
        defaultScene.lesson = lesson.id;
      }
      ps.push(new db.Scene(defaultScene).save());
    }
  }

  if (ps.length > 0) {
    try {
      await Promise.all(ps);
      log.info('Added ' + ps.length + ' default Scenes to database.');
    } catch (err) {
      log.error('There was an error adding default Scenes!');
      log.error(err);
    }
  }

  // Characters
  ps = [];
  log.info('Check characters');
  for (const charGroup of defaultCharacters) {
    if (charGroup.characters.length === 0) {
      log.warn('Scene ' + charGroup.scene + ' has no default characters set!');
      continue;
    }

    if (charGroup.scene) {
      const scene = await db.Scene.findOne({name: charGroup.scene});
      if (!scene) {
        log.warn('Scene ' + charGroup.scene + ' not found!');
        continue;
      }

      const characterCount = await db.Character.countDocuments({scene: scene.id});
      if (characterCount === 0) {
        log.info('No Characters found in database for scene ' + scene.name + ', adding defaults...');
        charGroup.characters.forEach(char => ps.push(new db.Character({...char, scene: scene.id}).save()));
      }
    } else if (charGroup.lesson) {
      const lesson = await db.Lesson.findOne({name: charGroup.lesson});
      if (!lesson) {
        log.warn('Lesson ' + charGroup.lesson + ' not found!');
        continue;
      }

      const characterCount = await db.Character.countDocuments({lesson: lesson.id});
      if (characterCount === 0) {
        log.info('No Characters found in database for lesson ' + lesson.name + ', adding defaults...');
        charGroup.characters.forEach(char => ps.push(new db.Character({...char, lesson: lesson.id}).save()));
      }
    }
  }

  if (ps.length > 0) {
    try {
      await Promise.all(ps);
      log.info('Added ' + ps.length + ' default Characters to database.');
    } catch (err) {
      log.error('There was an error adding default Characters!');
      log.error(err);
    }
  }

  // Problems
  ps = [];
  log.info('Check problems');
  for (const defaultProblem of defaultProblems) {
    let exists, lesson;
    if (defaultProblem.lesson) {
      lesson = await db.Lesson.findOne({name: defaultProblem.lesson}, {id: 1});
      if (!lesson) {
        log.error('Problem ' + defaultProblem.name + ' has an invalid lesson name: ' + defaultProblem.lesson);
        continue;
      }

      exists = await db.Problem.findOne({name: defaultProblem.name, lesson: lesson.id});
    } else {
      exists = await db.Problem.findOne({name: defaultProblem.name, lesson: null});
    }
    if (!exists) {
      log.info('Adding ' + defaultProblem.name + ' problem to DB.');
      if (defaultProblem.lesson) {
        defaultProblem.lesson = lesson.id;
      }
      ps.push(new db.Problem(defaultProblem).save());
    }
  }

  if (ps.length > 0) {
    try {
      await Promise.all(ps);
      log.info('Added ' + ps.length + ' default Problems to database.');
    } catch (err) {
      log.error('There was an error adding default Problems!');
      log.error(err);
    }
  }
};
