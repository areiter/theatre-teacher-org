const crypto = require('crypto');
const log = require('./lib/log').create('main');

let port;

const start = async () => {
  require('dotenv').config();
  port = process.env.port || 8080;

  log.info('Starting web server on port ' + port);

  if (!process.env.secret) {
    log.warn('No JWT secret found in env, generating single-use secret.');
    process.env.secret = crypto.randomBytes(24);
  }

  // Connect DB
  await require('./lib/db').connect();

  // Migrate DB
  await require('./lib/db-migrate')();

  // Start express server
  const app = await require('./lib/server').start();
  app.listen(port);
};

start().then(() => log.info('Web server started on port ' + port));
