import {enableProdMode} from "@angular/core";

enableProdMode();

export { SSRServerModule } from './ssr.server.module';
