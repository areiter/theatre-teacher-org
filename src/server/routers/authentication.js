const express = require('express');
const httpError = require('http-errors');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const nanoid = require('nanoid');
const AWS = require('aws-sdk');
const log = require('../lib/log').create('routers/authentication');
const middleware = require('../lib/middleware');
const util = require('../lib/util');
const db = require('../lib/db');

nanoid.customAlphabet('abcdefghijklmnopqrstuvwxyz1234567890', 8);

const router = express.Router();

AWS.config.update({
  region: 'us-east-1'
});

const SES =  new AWS.SES();

router.post('/login',
  //middleware.protect,
  async (req, res, cont) => {
    log.info('Logging in');
    try {
      const params = util.mapParams({
        username: 'req.body.username',
        password: 'req.body.password'
      }, req, res);

      const user = await db.User.findOne({$or: [{username: params.username}, {email: params.email}]});
      if (!user) {
        log.error('No user found with email/username' + params.username);
        return cont(httpError(403, {number: 4031, message: 'Sorry, that username and/or password is not working.'}));
      }

      const passwordValid = bcrypt.compareSync(params.password, user.password);
      if (!passwordValid) {
        log.error('Bad password attempt for user ' + user.id + ' (' + user.username + ').');
        return cont(httpError(403, {number: 4031, message: 'Sorry, that username and/or password is not working.'}));
      }

      user.lastLogin = new Date();
      await user.save();

      const refreshToken = crypto.randomBytes(32).toString('hex');
      const session = await new db.Session({
        user: user.id,
        refreshToken: refreshToken,
        expires: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 7)
      }).save();

      res.set('refreshtoken', refreshToken);
      res.set('accesstoken', jwt.sign({key: user.username, session: session.id}, process.env.secret, {
        expiresIn: 60 * 60 * 24
      }));

      log.info('User ' + user.id + ' (' + user.username + ') logged in, session ' + session.id + ' expires ' + session.expires.toISOString());

      res.body = user.sanitized();

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

router.post('/refresh',
  async (req, res, cont) => {
    log.info('Refreshing session');
    try {
      const params = util.mapParams({
        refreshToken: 'req.body.refreshToken'
      }, req, res);

      const session = await db.Session.findOne({refreshToken: params.refreshToken}).populate('user');
      if (!session) {
        log.warn('No session found with refresh token ' + params.refreshToken);
        return cont(httpError(403, {number: 4031, message: 'Refresh failed.'}));
      }

      // TODO: check expiration

      session.expires = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 7);
      await session.save();

      res.set('accesstoken', jwt.sign({
        key: session.user.username,
        session: session.id
      }, process.env.secret, {
        expiresIn: 60 * 60 * 24
      }));

      log.info('Session ' + session.id + ' refreshed');

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendSimpleSuccess
)

router.post('/logout',
  middleware.protect,
  async (req, res, cont) => {
    try {
      const params = util.mapParams({
        session: 'req.session',
        refreshToken: 'req.body.refreshToken'
      }, req, res);

      await params.session.remove();
      log.info('Deleted user ' + params.session.user.id + ' session ' + params.session.id);

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendSimpleSuccess
);

router.post('/reset-password',
  async (req, res, cont) => {
    try {
      const params = util.mapParams({
        username: 'req.body.username'
      }, req, res);

      const user = await db.User.findOne({$or: [{username: params.username}, {email: params.username}]});
      if (!user) {
        log.error('No user found with email/username' + params.username);
        return cont(httpError(403, {number: 4031, message: 'Sorry, that username/email is not working.'}));
      }

      if (!user.email) {
        log.error('User ' + user.id + ' does not have an email address registered.');
        return cont(httpError(400, {number: 4001, message: 'Sorry, you don\'t have an email address associated with your account.'}));
      }

      const token = nanoid.nanoid(8);

      log.info('Sending password reset email to ' + user.email);

      await SES.sendEmail({
        Source: 'no-reply@theatreteacher.org',
        Destination: {
          CcAddresses: ['alreit@gmail.com'],
          ToAddresses: [user.email],
        },
        Message: {
          Body: {
            Html: {
              Charset: 'UTF-8',
              Data: `
                  <p>There's been a password reset request for your theatreteacher.org account.</p>
                  <a href="https://theatreteacher.org/account/reset?t=${token}">Click here to reset your password.</a>
                  <p>If you didn't make this request or don't need to reset your password, you can ignore this email.</p>
                `
            }
          },
          Subject: {
            Charset: 'UTF-8',
            Data: 'theatreteacher.org Password Reset'
          }
        }
      }).promise();

      await new db.Temp({
        key: token,
        value: user.id
      }).save();

      log.info('Password reset email sent, token created.');

      cont();
    } catch (err) {
      console.error(err);
      cont(err);
    }
  },
  middleware.sendSimpleSuccess
);

router.post('/validate-token',
  async (req, res, cont) => {
    try {
      const params = util.mapParams({
        token: 'req.body.token'
      }, req, res);

      const temp = await db.Temp.findOne({key: params.token});
      if (!temp) {
        log.error('No temp found with key ' + params.token);
        return cont(httpError(404, {number: 4041, message: 'Reset token is invalid or has already been used.'}))
      }

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendSimpleSuccess
);

router.post('/change-password',
  async (req, res, cont) => {
    try {
      const params = util.mapParams({
        password: 'req.body.password',
        token: 'req.body.token'
      }, req, res);

      const temp = await db.Temp.findOne({key: params.token});
      if (!temp) {
        log.error('No temp found with key ' + params.token);
        return cont(httpError(404, {number: 4041, message: 'Reset token is invalid or has already been used.'}))
      }

      const user = await db.User.findById(temp.value);
      if (!user) {
        log.error('No user found with id ' + temp.value);
        return cont(httpError(404, {number: 4041, message: 'Reset token is invalid or has already been used.'}))
      }

      user.password = bcrypt.hashSync(params.password, 10);
      await user.save();

      log.info('User ' + user.id + ' password updated, deleting temp ' + temp.id);

      await temp.remove();

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendSimpleSuccess
);

router.post('/update-password',
  middleware.protect,
  async (req, res, cont) => {
    try {
      const params = util.mapParams({
        user: 'req.session.user',
        password: 'req.body.password'
      }, req, res);

      params.user.password = bcrypt.hashSync(params.password, 10);
      await params.user.save();

      log.info('User ' + params.user.id + ' password updated');

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendSimpleSuccess
);

module.exports = router;
