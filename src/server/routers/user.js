const express = require('express');
const bcrypt = require('bcryptjs');
const log = require('../lib/log').create('routers/authentication');
const middleware = require('../lib/middleware');
const util = require('../lib/util');
const db = require('../lib/db');
const httpError = require('http-errors');

const router = express.Router();

const reserved = [

];

router.get('/test', (req, res, cont) => res.send(new Date()));

router.put('/',
  //middleware.protect,
  async (req, res, cont) => {
    log.info('Creating a new user account');
    try {
      const params = util.mapParams({
        username: 'req.body.username',
        password: 'req.body.password',
        'email?': 'req.body.email',
        firstName: 'req.body.firstName',
        'lastName?': 'req.body.lastName',
        teacher: 'req.body.teacher'
      }, req, res);

      params.username = params.username.toLowerCase();
      const usernameTaken = await db.User.countDocuments({username: params.username});
      if (usernameTaken || reserved.indexOf(params.username) !== -1) {
        return cont(httpError(409, {number: 4091, message: 'Sorry!  That username is unavailable.  Please choose a different username.'}));
      }


      if (params.email) {
        params.email = params.username.toLowerCase();
        const emailTaken = await db.User.countDocuments({email: params.email});
        if (emailTaken) {
          return cont(httpError(409, {number: 4091, message: 'Sorry!  That email is already taken.  Are you sure you don\' have an account already?'}));
        }
      }

      const passwordHashed = bcrypt.hashSync(params.password, 10);

      const user = await new db.User({
        username: params.username,
        email: params.email,
        password: passwordHashed,
        firstName: params.firstName,
        lastName: params.lastName,
        teacher: params.teacher
      }).save();

      log.info('New user ' + user.id + ' (' + user.username + ') created!');

      res.body = user.sanitized();

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

router.post('/delete',
  middleware.protect,
  async (req, res, cont) => {
    try {
      const params = util.mapParams({
        user: 'req.session.user'
      }, req, res);

      const user = await db.User.findById(params.user.id);
      if (!user) {
        log.error('No user found with id ' + params.user.id);
        return cont(httpError(404, {number: 4041, message: 'User already deleted.'}))
      }

      await user.remove();
      await db.Play.deleteMany({user: user.id});

      log.info('User ' + user.id + ' deleted');

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendSimpleSuccess
);

module.exports = router;
