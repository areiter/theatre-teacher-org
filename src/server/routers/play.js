const express = require('express');
const log = require('../lib/log').create('routers/play');
const middleware = require('../lib/middleware');
const util = require('../lib/util');
const db = require('../lib/db');
const dot = require('dot');
const fs = require('fs');
const path = require('path');
const httpError = require('http-errors');

const router = express.Router();
const playTemplate = dot.template(fs.readFileSync(path.join(__dirname, '../templates/play.html')));

router.get('/my-list',
  middleware.protect,
  async (req, res, cont) => {
    log.info('Reading user plays');
    try {
      const params = util.mapParams({
        session: 'req.session'
      }, req, res);

      const plays = await db.Play.find({user: params.session.user.id}).populate([
        {path: 'user', select: '-password'},
        {path: 'sharedWith', select: '-password'},
        {path: 'scene.template'},
        {path: 'characters.template'},
        {path: 'problem.template'}
      ]);

      log.info('Found ' + plays.length + ' plays by user ' + params.session.user.id);

      res.body = plays;

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

router.get('/lessons',
  //middleware.protect,
  async (req, res, cont) => {
    log.info('Reading play lessons');
    try {
      const lessons = await db.Lesson.find();

      log.info('Found ' + lessons.length + ' play lessons.');

      res.body = lessons;

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

router.get('/scenes',
  //middleware.protect,
  async (req, res, cont) => {
    log.info('Reading play scenes');
    try {
      const params = util.mapParams({
        'user?': 'req.query.user',
        'lesson?': 'req.query.lesson'
      }, req, res);

      const query = {};
      if (params.user) {
        query.user = params.user;
      }

      if (params.lesson) {
        query.lesson = params.lesson;
      } else {
        query.lesson = null;
      }

      const scenes = await db.Scene.find(query).populate([
        {path: 'user', select: '-password'},
        {path: 'characters'}
      ]);

      log.info('Found ' + scenes.length + ' scenes.');

      res.body = scenes;

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

router.get('/characters',
  //middleware.protect,
  async (req, res, cont) => {
    log.info('Reading play characters');
    try {
      const params = util.mapParams({
        'lesson?': 'req.query.lesson'
      }, req, res);

      if (params.lesson) {
        const characters = await db.Character.find({lesson: params.lesson});

        log.info('Found ' + characters.length + ' characters from lesson ' + params.lesson);

        res.body = characters;
      } else {
        const scenes = await db.Scene.find({lesson: null}).populate({
          path: 'characters'
        });

        log.info('Found ' + scenes.length + ' scenes with characters.');

        res.body = scenes;
      }

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

router.get('/problems',
  //middleware.protect,
  async (req, res, cont) => {
    log.info('Reading play problems');
    try {
      const params = util.mapParams({
        'lesson?': 'req.query.lesson'
      }, req, res);

      const query = {};
      if (params.lesson) {
        query.lesson = params.lesson;
      } else {
        query.lesson = null;
      }

      const problems = await db.Problem.find(query);

      log.info('Found ' + problems.length + ' problems.');

      res.body = problems;

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

router.post('/print',
  async (req, res, cont) => {
    log.info('Generating printable play');
    try {
      const params = util.mapParams({
        'user?': 'req.query.user',
        play: 'req.body.play'
      }, req, res);

      res.body = {html: playTemplate(params.play)};

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

router.post('/save',
  middleware.protect,
  async (req, res, cont) => {
    log.info('Saving play');
    try {
      const params = util.mapParams({
        play: 'req.body.play',
        session: 'req.session'
      }, req, res);

      if (!params.play.user) {
        params.play.user = params.session.user;
      }

      let play;
      if (params.play.id) {
        play = await db.Play.findById(params.play.id);
      }

      if (!play) {
        log.info('Play does not exist, creating a new one.');
        play = new db.Play(params.play);
      } else {
        log.info('Play found with id ' + params.play.id + ', saving new document.');
        delete params.play.id;
        Object.assign(play, params.play);
      }

      if (play.state === 'in-progress') {
        play.state = 'complete';
      }

      await play.save();

      log.info('Play ' + play.id + ' saved.');

      res.body = play;

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

router.post('/delete',
  middleware.protect,
  async (req, res, cont) => {
    log.info('Deleting play');
    try {
      const params = util.mapParams({
        playId: 'req.body.playId',
        session: 'req.session'
      }, req, res);

      const play = await db.Play.findById(params.playId);

      if (!play) {
        log.error('No play found with id ' + params.playId);
        return cont(httpError(404, {message: 'Play not found.', number: 4041}));
      }

      await play.remove();

      log.info('Play ' + play.id + ' deleted.');

      res.body = true;

      cont();
    } catch (err) {
      cont(err);
    }
  },
  middleware.sendJSONResponse
);

module.exports = router;
