FROM node:14-alpine3.10

ARG node_environment=dev
ARG mongo_cluster
ARG mongo_user
ARG mongo_db
ARG mongo_password

ENV NODE_ENV $node_environment
ENV mongo_cluster ${mongo_cluster}
ENV mongo_user ${mongo_user}
ENV mongo_db ${mongo_db}
ENV mongo_password ${mongo_password}

ENV PORT 8080

WORKDIR /opt/theatre-teacher-org/

ADD src /opt/theatre-teacher-org/src/
ADD angular.json /opt/theatre-teacher-org
ADD tsconfig.json /opt/theatre-teacher-org
ADD package.json /opt/theatre-teacher-org
ADD package-lock.json /opt/theatre-teacher-org
ADD Dockerrun.aws.json /opt/theatre-teacher-org

RUN ["npm", "install", "--production", "--no-audit"]
RUN ["mkdir", "/opt/theatre-teacher-org/dist"]
RUN ["npm", "run", "build"]
RUN ["rm", "-rf", "src", "angular.json", "tsconfig.json"]

RUN ["sh", "-c", "mv /opt/theatre-teacher-org/dist/* /opt/theatre-teacher-org"]
RUN ["rm", "-rf", "dist"]

RUN echo The node environment is $NODE_ENV
RUN echo Mongo cluster is $mongo_cluster

EXPOSE 8080
CMD npm run start:prod
